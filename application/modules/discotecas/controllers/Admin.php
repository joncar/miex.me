<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function discotecas(){            
            $crud = $this->crud_function('','');            
            /*$crud->unset_add()->unset_delete();*/
            $crud->field_type('coordenadas','map',array('width'=>'100%','height'=>'300px'));
            $crud->set_field_upload('logo','images/discotecas');
            $crud->set_field_upload('banner','images/discotecas');
            $crud = $crud->render();
            $crud->title = 'Registro de discotecas';                        
            $this->loadView($crud);
        }
    }
?>
