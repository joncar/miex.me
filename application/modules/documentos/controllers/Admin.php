<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function documentos(){            
            $crud = $this->crud_function('',''); 
            $crud->set_field_upload('file','files');
            $crud = $crud->render();
            $crud->title = 'Registro de pagos';                        
            $this->loadView($crud);
        }
    }
?>
