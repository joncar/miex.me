<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }

        function categorias_galeria($x = '',$y = ''){
            $crud = $this->crud_function('','');  
            $crud->add_action('<i class="fa fa-image"></i> Imagenes','',base_url('galeria/admin/galeria').'/');          
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        function galeria(){
            $this->load->library('image_crud');
            $crud = new image_CRUD();
            $crud->set_table('galeria')
                 ->set_image_path('images/galeria')
                 ->set_ordering_field('priority')
                 ->set_url_field('foto')
                 ->set_title_field('titulo')
                 ->set_relation_field('categorias_galeria_id')
                 ->set_subject('Galería');
            $crud->module = 'galeria';
            $crud = $crud->render();
            $crud->title = 'Galería Fotográfica';            
            $crud->loadJquery = true;
            $this->loadView($crud);
        }
    }
?>
