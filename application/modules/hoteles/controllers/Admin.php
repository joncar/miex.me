<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function categorias(){
            $this->as['categorias'] = 'categorias_hoteles';
            $crud = $this->crud_function('','');
            $crud->columns('categorias_hoteles_nombre');            
            $crud = $crud->render();
            $this->loadView($crud);
        }
        function hoteles(){
            $crud = $this->crud_function('','');
            
            $crud->set_field_upload('portada','images/hoteles');
            $crud->field_type('ubicacion','map',array('width'=>'100%','height'=>'300px'));
            $crud->add_action('<i class="fa fa-image">Galería de fotos</i>','',base_url('hoteles/admin/fotos').'/');
            $crud->add_action('<i class="fa fa-wrench">Servicios</i>','',base_url('hoteles/admin/servicios').'/');
            $crud = $crud->render();
            $crud->output.= $this->load->view('backend/crud',array(),TRUE);
            $this->loadView($crud);
        }
        
        function servicios($x = ''){
            if(is_numeric($x)){
                $this->as['servicios'] = 'hoteles_servicios';
                $crud = $this->crud_function('','');
                $crud->where('hoteles_id',$x);
                $crud->field_type('hoteles_id','hidden',$x);
                $crud = $crud->render();                
                $this->loadView($crud);
            }
        }
        
        function fotos(){
            $this->load->library('image_crud');
            $crud = new image_CRUD();
            $crud->set_table('hoteles_fotos')
                 ->set_image_path('images/hoteles')
                 ->set_ordering_field('priority')
                 ->set_relation_field('hoteles_id')
                 ->set_url_field('foto')
                 ->set_subject('Galería');
            $crud->module = 'hoteles';            
            $crud = $crud->render();
            $crud->title = 'Galería Fotográfica';            
            $crud->loadJquery = true;
            $this->loadView($crud);
        }
    }
?>
