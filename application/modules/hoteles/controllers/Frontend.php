<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Frontend extends Main{
        function __construct() {
            parent::__construct();
        }
        
        function index(){
            $blog = new Bdsource();
            //$blog->limit = array('6','0');            
            if(!empty($_GET['descripcion'])){
                $blog->like('nombre',$_GET['descripcion']);
            }
            if(!empty($_GET['categorias_hoteles_id'])){
                $blog->where('categorias_hoteles_id',$_GET['categorias_hoteles_id']);
            }
            //$blog->where('idioma',$_SESSION['lang']);
            if(!empty($_GET['page'])){
                //$blog->limit = array(($_GET['page']-1),6);
            }
            $blog->order_by = array('orden','ASC');
            $blog->init('hoteles');
            $totalpages = round($this->db->get_where('hoteles')->num_rows()/6);
            $totalpages = $totalpages==0?'1':$totalpages;
            
            $this->loadView(
            array(
                'view'=>'frontend/main',
                'detail'=>$this->hoteles,
                'total_pages'=>$totalpages,
                'title'=>'Hoteles',
                'categorias'=>$this->get_categorias()
            ));
        }
        
        function get_categorias(){
            $c = $this->db->get('categorias_hoteles');
            foreach($c->result() as $n=>$v){
                $c->row($n)->cantidad = $this->db->get_where('hoteles',array('categorias_hoteles_id'=>$v->id))->num_rows();
            }
            return $c;
        }
        
        public function read($id){
            $id = explode('-',$id);
            $id = $id[0];
            if(is_numeric($id)){
                $blog = new Bdsource();
                $blog->where('id',$id);
                $blog->init('hoteles',TRUE);                
                $this->loadView(
                    array(
                        'view'=>'frontend/detail',
                        'detail'=>$this->hoteles,
                        'title'=>$this->hoteles->nombre,                        
                        'categorias'=>$this->get_categorias(),
                        'servicios'=>$this->db->get_where('hoteles_servicios',array('hoteles_id'=>$id))
                    ));
            }else{
                throw new Exception('No se encuentra la entrada solicitada',404);
            }
        }
    }
?>
