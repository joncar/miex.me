<script>
var input = document.getElementById('field-direccion'); 
var autocomplete = new google.maps.places.Autocomplete(input,{types: ['geocode']});
autocomplete.addListener('place_changed', fillInAddress);
function fillInAddress(){    
    var place = autocomplete.getPlace().geometry.location;    
    ubicacion.marker.setPosition(new google.maps.LatLng(place.lat(),place.lng()));
    ubicacion.map.panTo(new google.maps.LatLng(place.lat(),place.lng()));
    $("#field-ubicacion").val(ubicacion.marker.getPosition());
}
</script>