<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }

        function categorias_paginas($x = '',$y = ''){
            $crud = $this->crud_function('','');            
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        function paginas($x = '',$y = ''){
            $crud = $this->crud_function('','');
            $crud->columns('categorias_paginas_id','titulo');            
            $crud->field_type('subtitulo','hidden','')
                 ->field_type('precio','hidden','');
            if($x=='upload_file' || $x == 'delete_file'){
                $crud->set_field_upload('video_id_youtube','img');
            }
            if($crud->getParameters()=='edit' && is_numeric($y)){
                $pagina = $this->db->get_where('paginas',array('id'=>$y));
                if($pagina->num_rows()>0){
                    $pagina = $pagina->row();

                    //Si es actividades extras
                    if($y=='4'){
                        $crud->field_type('video_id_youtube','string');
                    }

                    //Si es seguros
                    if($y=='5' || $y=='6'){
                        $crud->field_type('precio','string')
                             ->field_type('subtitulo','string');   
                        $crud->field_type('video_id_youtube','hidden');
                    }

                    if($y=='11'){
                        $crud->field_type('precio','string')
                             ->field_type('subtitulo','string')
                             ->field_type('precio','hidden');   
                        $crud->set_field_upload('video_id_youtube','img')
                             ->display_as('video_id_youtube','Imagen');
                    }

                    else{
                        $crud->field_type('video_id_youtube','hidden');
                    }
                    


                }
            }
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        function banner(){
            $crud = $this->crud_function('','');            
            $crud->columns('foto');
            if($crud->getParameters()!='list'){
                $crud->field_type('foto','image',array('width'=>'600px','height'=>'300px','path'=>'img/banner'));
            }else{
                $crud->set_field_upload('foto','img/banner');
            }
            $crud->field_type('posicion','dropdown',
                    array(
                        'top:50px; left:0px;'=>'Izquierda - Arriba',
                        'top:160px; left:0px;'=>'Izquierda - Centro',
                        'bottom:0px; left:0px;'=>'Izquierda - Abajo',                        
                        'top:50px; left:30%;'=>'Centro - Arriba',
                        'top:160px; left:30%;'=>'Centro - Centro',
                        'bottom:0px; left:30%;'=>'Centro - Abajo',                        
                        'top:50px; right:0; left:initial;'=>'Derecha - Arriba',
                        'top:160px; right:0; left:initial;'=>'Derecha - Centro',
                        'bottom:0px; right:0; left:initial;'=>'Derecha - Abajo'
                    ));
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        function subscriptores(){
            $crud = $this->crud_function('','');          
            $crud = $crud->render();
            $this->loadView($crud);
        }

        function testimonios(){
            $crud = $this->crud_function('','');            
            
            $crud->field_type('foto_autor','image',array('path'=>'img/testimonios','width'=>'120px','height'=>'120px'));
            $crud->field_type('foto_banner','image',array('path'=>'img/testimonios','width'=>'370px','height'=>'180px'));
            $crud->field_type('estrellas','enum',array('1','2','3','4','5'));
            /*$crud->field_type('testimonio','editor',array('type'=>'textarea'));*/
            $crud = $crud->render();
            $this->loadView($crud);
        }
    }
?>
