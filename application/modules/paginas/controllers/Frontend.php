<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Frontend extends Main{
        function __construct() {
            parent::__construct();
        }        
        
        function read($url){            
            $this->loadView(array('view'=>'read','page'=>$this->load->view($url,array(),TRUE),'title'=>ucfirst(str_replace('-',' ',$url))));
        }
        
        function getFormReg($x = '2'){                    
            return $this->querys->getFormReg($x);
        }
        
        function subscribir(){
            $this->load->library('form_validation');
            $this->form_validation->set_rules('email','Email','required|valid_email');
            if($this->form_validation->run()){
                $email = $this->db->get_where('subscriptores',array('email'=>$_POST['email']));
                if($email->num_rows()==0){
                    $this->db->insert('subscriptores',array('email'=>$_POST['email']));
                    echo $this->success('Subscrito satisfactoriamente');
                }else{
                    echo $this->error('El correo ya se encuentra subscrito');
                }
            }else{
                echo $this->error($this->form_validation->error_string());
            }
        }
    }
?>
