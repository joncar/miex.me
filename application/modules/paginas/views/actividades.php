<?php $d = $this->db->get('discotecas'); ?>
<!-- page content --> 
<div class="tour-page loaded" id="page-content"> 
    <header id="tour-header" data-bg="<?= base_url().'img/'.$this->db->get_where('paginas',array('id'=>11))->row()->video_id_youtube ?>" class="overlay" style="background-image: url(<?= base_url().'img/'.$this->db->get_where('paginas',array('id'=>11))->row()->video_id_youtube ?>);">
        <!-- navigation / main menu --> 
        <nav class="navbar navbar-fixed-top"> 
            <div class="container"> 
                <div class="navbar-header"> 
                    <button aria-expanded="false" data-target="#myMenu" data-toggle="collapse" class="navbar-toggle collapsed" type="button"> 
                        <span class="sr-only">Toggle navigation</span> 
                        <span class="icon-bar"></span> 
                        <span class="icon-bar"></span> 
                        <span class="icon-bar"></span> 
                    </button> 
                    <a href="<?= site_url() ?>" class="navbar-brand"> 
                        <img src="<?= base_url() ?>images/logopetit.jpg" style="width:180px">
                    </a>
                </div> <!-- navbar-collapse --> 
                <div id="myMenu" class="collapse navbar-collapse"> 
                    <ul class="nav navbar-nav text-uppercase text-center"> 
                        <?= $this->load->view('includes/template/menu-item') ?>
                    </ul> 
                    <button aria-expanded="false" data-target="#second-nav" data-toggle="collapse" class="second-nav-button navbar-toggle collapsed" type="button"> 
                        <span class="sr-only">Toggle navigation</span> 
                        <span class="icon-bar"></span> 
                        <span class="icon-bar"></span> 
                        <span class="icon-bar"></span> 
                    </button> 
                </div> <!-- /.navbar-collapse --> 
            </div>
            <div class="animated slideInDown collapse navbar-collapse" id="second-nav"> <!-- container --> 
                <div class="container"> 
                    <div class="row"> 
                        <ul role="tablist" class="col-md-12 list-inline nav nav-tabs"> 
                            <li class="active"> 
                                <a href="#trip-actividades-incluidas">Incluidas</a>
                            </li> 
                            <li class=""> 
                                <a href="#trip-actividades-extras">Extras</a>
                            </li> 
                            <li class=""> 
                                <a href="#trip-alojamientos">Alojamientos</a>
                            </li> 
                            <li class=""> 
                                <a href="#trip-discotecas">Discotecas</a>
                            </li> 
                            <li class=""> 
                                <a href="#trip-seguros">Seguros</a>
                            </li> 
                            <li> 
                                <a href="#trip-material-viaja-gratis">Material</a>
                            </li> 
                            <li> 
                                <a href="#trip-autorizacion-paterna">Condiciones</a>
                            </li> 
                            <li> 
                                <a href="#trip-preguntas-frecuentes">Preguntas</a>
                            </li> 
                            <li class="text-uppercase pull-right"> 
                                <a href="<?= site_url('p/contactenos') ?>">Contrátalo</a>
                            </li> 
                        </ul> 
                    </div> 
                </div> <!-- /.container --> 
            </div> 
        </nav> 
        <div data-wow-duration="1s" data-wow-delay="0.8s" class="header-center-content animated fadeIn"> 
            <div class="container"> 
                <div class="row text-center"> 
                    <div class="col-md-12"> 
                        <i class="icon icon-caution"style=" font-size: 60px; color: #f71259;"></i>
                        <h1 class="text-uppercase"><?= $this->db->get_where('paginas',array('id'=>11))->row()->subtitulo ?></h1> 
                        <p style="margin: 0px;"> 
                            <a class="header-tag" href="http://miex.me/p/contactenos"> ¿Tienes alguna duda?</a>
                        </p>
                        <p class="coma" style="margin: 0px;">inapproachability</p>
                        <span class="poll">monogamous incardinating</span> Contacta 
                        <a class="header-tag" href="http://miex.me/p/contactenos">
                            con nosotros
                        </a>
                        <p class="vice" style="margin: 0px;">hyperelegance</p>
                        <span class="sown">undermelody deglutination</span> 
                        <p style="margin: 0px;"></p>
                        <p class="poll" style="margin: 0px;">verrucosity</p>
                        <span class="libeled">extravagance monogyny</span>
                        <span class="vice">recancellation</span> 
                    </div> 
                </div> 
            </div> 
        </div> 
        <div data-wow-delay="0.4s" class="trip-costs animated slideInUp"> 
            <div class="container text-center"> 
                <div class="row"> 
                    <?= $this->db->get_where('paginas',array('id'=>11))->row()->contenido ?>
                </div> 
            </div> 
        </div> 
    </header> <!-- main content --> 
    <main> 
        <section id="trip-experience"> 
            <div class="container"> <!-- section-intro --> 
                <div class="row text-center section-intro bordered">
                    <div class="col-md-12"> 
                        <i class="icon icon-paperclip wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;"></i> 
                        <h1 class="text-uppercase wow fadeInUp" style="visibility: visible; animation-name: fadeInUp;">
                            Qué incluye
                        </h1> 
                        <p class="wow fadeInUp" style="margin: 0px; visibility: visible; animation-name: fadeInUp;">
                            MALLORCA ISLAND EXPERIENCE 2017
                        </p>
                    </div> 
                </div> <!-- /.section-intro --> <!-- small intro --> 
                <div class="row"> 
                    <div class="col-md-offset-1 col-md-10">
                        <div class="small-intro">
                            <p style="margin: 0px;">
                            <h3>Vuelo  IDA + VUELTA EN COMPAÑÍA REGULAR</h3>
							<h4>(VUELING, VOLOTEA, AIR EUROPA O IBERIA)</h4>
							
							• Transporte de equipaje de cabina hasta 10 kg y de bodega hasta 23kg.</br>
							• Mejor acomodación disponible.
 
							<h3>TRANSFERS</h3>
							• Traslado autobús - Instituto / aeropuerto de salida. (OPCIONAL)</br>
							• Traslado autobús – aeropuerto de Mallorca / Hotel, ida y vuelta. (INCLUIDO)</br></br>
							Sabemos que lo que más cuesta a los estudiantes cuando intentan organizarse entre ellos a la hora de buscar su viaje de fin de estudios es cómo llegar a los destinos (horarios, puntos de encuentro, autobuses, aviones, barcos, equipajes, bonos de reserva, documentación, traslados, etc.).
Es en este punto donde hacemos especial relevancia al trabajo de nuestro equipo técnico.
El viaje se iniciará como punto de encuentro en la localidad de su hijo, donde nuestro autobús, acompañado de nuestro staff, recogerá a todo su grupo el día de salida a la hora establecida.
Antes del viaje nuestro equipo técnico les dará la información y documentación final del viaje, la pulsera identificativa, etc… según corresponda.</br></br>
Al subir al autobús nuestro equipo le dará la bienvenida al grupo entregándole la documentación del viaje: tarjeta de embarque, y demás.
Una vez estacione el autobús en el aeropuerto, todos los grupos que inicien la salida ese mismo día únicamente tendrán que seguir las indicaciones de nuestro staff procediendo a la facturación de sus maletas y al embarque, así de sencillo, para los estudiantes y para la organización.
En el trayecto en avión, sus hijos irán acompañados hasta el aeropuerto de Palma de Mallorca, lugar donde nuevamente nuestro equipo les estará esperando con los autobuses para guiarles hasta el Resort.
Como decíamos al principio, los traslados son así de sencillos. Los estudiantes y sus padres no deben preocuparse de nada durante los trayectos ya que en todo momento sus hijos podrán realizar directamente cualquier consulta que puedan tener a nuestros miembros de la organización, ya que les acompañan en todo momento.
 
							<h3>TRANSFERS ALOJAMIENTO en hoteles de  *** y ***sup.</h3>
							Todos los estudiantes se alojarán en habitaciones de los hoteles adecuadas al uso de los estudiantes, suelen estar completamente equipados con tv, baño privado, terraza y las habitaciones suelen ser triples o cuádruples (es decir, múltiples). 
Como en el viaje está incluida la Pensión Completa, no se tendrán que preocupar de nada, ya que el hotel dispone de un restaurante buffet libre para que todos los estudiantes puedan desayunar, comer y cenar. A parte de estos hoteles tenemos otros en los que su régimen alimenticio es el Todo incluido, y se indicará horarios y contenidos dependiendo del hotel elegido ya que puede tener variaciones de unos a otros.
</br></br>
							• Habitaciones  en distribución múltiple.</br>
							• Pensión completa o Todo Incluido según se elija.
							
							<h3>STAFF EN ORIGEN: 2 tipos.</h3>
Con bus traslado aeropuerto ida/vuelta pagando un suplemento asistencia desde el punto de encuentro por parte de nuestro personal.</br></br>
Sin bus, asistencia en aeropuerto ida/vuelta por parte de nuestro personal.

							<h3>STAFF EN MALLORCA: Asistencia en el traslado, estancia en el hotel, tfno. 24 horas estudiantes, teléfono urgencias padres, un asistente de referencia por hotel.</h3>
Les darán la bienvenida a sus hijos a la llegada a Mallorca en el Aeropuerto y se encargarán que toda la organización del viaje se cumpla a la perfección.
Los acompañarán en el traslado a los hoteles y en el check-in para el que tendrán que tener a mano su DNI, y el depósito que solicita el Hotel.</br></br>

							<h3>OTROS GASTOS: EQUIPAJE, IMPUESTOS Y TASAS</h3>
Especificar que el equipaje indicado en caso de vuelo indirecto es sólo cabina, con vuelo directo salvo indicación e contrario es con maleta de cabina y facturada.</br> 
Los impuestos, tasas y carburante están incluidos y sólo se podrán cobrar en caso de que Aena o la compañía aérea lo haga.</br></br>

							<h3>SEGUROS</h3>
							Dos tipos de seguros para que estés bien cubierto.</br></br>
                                                        <a href="http://miex.me/assets/cobertura_Multiasistencia_Plus_Elite.pdf"> COBERTURA MULTIASISTENCIA PLUS ELITE.pdf</br></a>
                                                        <a href="http://miex.me/assets/cobertura_AXA_Assistance_Estudiantes.pdf">COBERTURA AXA ASSISTANCE ESTUDIANTES.pdf</br></br></a>
                            </p>
                        </div> 
                    </div> 
                </div> <!-- /.small intro --> <!-- trip background -->                 
            </div> 
        </section> 
        <section id="trip-activities" style="margin:85px 0"> <!-- trip activities background --> 
            <div data-bg="<?= base_url() ?>images/trip-activities-bg.jpg" class="trip-activities-bg wow slideInLeft" style="background-image: url(<?= base_url() ?>images/trip-activities-bg.jpg); visibility: visible; animation-name: slideInLeft;">
                
            </div> <!-- /.trip activities background --> 
            <div class="container"> 
                <div class="row">
                    <div class="col-sm-4 col-md-6">

                    </div>
                    <div class="col-sm-8 col-md-6">
                        <div class="row"> <!-- trip activities --> 
                            <div class="col-md-offset-2 col-md-10 col-sm-offset-6 col-sm-6"> 
                                <div class="section-intro"> 
                                    <i data-wow-delay="0.4s" class="icon icon-pricetags wow fadeInLeft" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInLeft;"></i> 
                                    <h1 data-wow-delay="0.5s" class="text-uppercase wow fadeInLeft" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInLeft;">
                                        Actividades Incluidas
                                    </h1> 
                                    <p data-wow-delay="0.6s" class="wow fadeInLeft" style="margin: 0px; visibility: visible; animation-delay: 0.6s; animation-name: fadeInLeft;">
                                        Durante la estancia de sus hijos nuestra organización tiene preparadas un sinfín de actividades para todos los grupos con el fin de hacer de su viaje de fin de estudios una experiencia inolvidable.
                                        El primer día a la llegada al hotel elegido, nuestro staff les acompañará en el check-in, les atenderá e intentará resolver todas las dudas que puedan tener, prestándoles asistencia 24 horas.
                                        Además les situarán en el mapa, explicarán las actividades, excursiones y ocio que podrán disfrutar durante sus estancias y facilitarán consejos e información práctica a tener en cuenta, como listados telefónicos, puntos de encuentro, lugares de interés, centros de salud, policía, información turística etc.
                                        Todas las actividades son voluntarias y opcionales y nuestro equipo técnico y staff se encargará de que todos los estudiantes puedan disfrutar de la combinación de eventos deportivos, conciertos, excursiones, playas y sol.
                                        Les informaremos de los teléfonos de referencia de una persona del Staff por hotel, y además estaremos en continua convivencia con ellos en su día a día y a su entera disposición.

                                    </p>
                                </div> 
                            </div> 
                        </div> 
                        <div class="row"> 
                            <div class="col-md-offset-2 col-md-10 col-sm-offset-6 col-sm-6"> 
                                <ul data-wow-delay="0.2s" class="list-unstyled activities-list wow slideInRight" style="visibility: visible; animation-delay: 0.2s; animation-name: slideInRight;"> 
                                    <li> 
                                        <h4>Pool Parties</h4> 
                                        <span>Con barra libre de cócteles.* (Según hoteles y disponibilidad).</span>                                
                                    </li> 
                                    <li> 
                                        <h4>Videos</h4> 
                                        <span>Aftermovie & fotografías del evento.</span>                                
                                    </li> 
                                    <li> 
                                        <h4>Regalos sorpresa</h4> 
                                        <span>Durante el evento. (Participa en los diferentes concursos y eventos)</span>                                
                                    </li> 
                                </ul> 
                            </div> 
                            <div class="col-md-offset-2 col-md-10 col-sm-offset-6 col-sm-6"> 
                                <ul data-wow-delay="0.3s" class="list-unstyled activities-list bordered wow slideInRight" style="visibility: visible; animation-delay: 0.3s; animation-name: slideInRight;"> 
                                    <li> 
                                        <h4>Asistencia</h4> 
                                        <span>Los estudiantes tendrán a su disposición un equipo de monitores para lo que necesiten.</span>                                
                                    </li> 
                                    <li> 
                                        <h4>Actividades diurnas opcionales </h4> 
                                        <span>Posibilidad de visitar la isla y la ciudad de Palma con nosotros.</span>
                                    </li> 
                                    <li> 
                                        <h4>Otras actividades</h4> 
                                        <span>También disponemos de actividades extra para completar tu estancia.</span>
                                    </li> 
                                </ul> 
                            </div> <!-- /.trip activities --> 
                        </div> 
                    </div>
                </div>
            </div> 
        </section> 
        <section style="margin-top:-85px">
            <iframe width="100%" height="315" src="https://www.youtube.com/embed/4no-t3C4H1Q" frameborder="0" allowfullscreen></iframe>
        </section>
        <section id="trip-actividades-extras" style="margin:85px 0;"> <!-- trip activities background --> 
            <div data-bg="<?= base_url() ?>images/boat.jpg" class="trip-activities-bg wow slideInLeft" style="background-image: url(<?= base_url() ?>images/boat.jpg); visibility: visible; animation-name: slideInLeft;">
                
            </div> <!-- /.trip activities background --> 
            <div class="container"> 
                <div class="row">
                    <div class="col-sm-4 col-md-6">

                    </div>
                    <div class="col-sm-8 col-md-6">
                        <?= $this->db->get_where('paginas',array('id'=>4))->row()->contenido; ?>
                    </div>
                </div>
            </div> 
        </section> 
        <section style="margin-top:-85px">
            <iframe width="100%" height="315" src="https://www.youtube.com/embed/<?= $this->db->get_where('paginas',array('id'=>4))->row()->video_id_youtube; ?>" frameborder="0" allowfullscreen></iframe>
        </section>
        <section id="trip-alojamientos" style="margin:85px 0;"> <!-- trip activities background --> 
            <div data-bg="<?= base_url() ?>images/alojamiento.jpg" class="trip-activities-bg wow slideInLeft" style="background-image: url(<?= base_url() ?>images/alojamiento.jpg); visibility: visible; animation-name: slideInLeft;">
                
            </div> <!-- /.trip activities background --> 
            <div class="container"> 
                <div class="row">
                    <div class="col-sm-4 col-md-6">

                    </div>
                    <div class="col-sm-8 col-md-6">
                        <div class="row"> <!-- trip activities --> 
                            <div class="col-md-offset-2 col-md-10 col-sm-offset-6 col-sm-6"> 
                                <div class="section-intro"> 
                                    <i data-wow-delay="0.4s" class="icon icon-key wow fadeInLeft" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInLeft;"></i> 
                                    <h1 data-wow-delay="0.5s" class="text-uppercase wow fadeInLeft" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInLeft;">
                                        Alojamientos
                                    </h1> 
                                    <p data-wow-delay="0.6s" class="wow fadeInLeft" style="margin: 0px; visibility: visible; animation-delay: 0.6s; animation-name: fadeInLeft;">
                                        Los hoteles principalmente se encuentran en la zona del Arenal, una de las más visitadas y especiales playas de Mallorca, con su puerto deportivo y mucho ambiente diurno y nocturno. Pudiendo elegir también la zona de Magaluf de así desearlo.
                                        En la zona tienen todos los servicios, centro de salud, ayuntamiento, guardia civil, bares, discotecas, pubs, tiendas, supermercados,  cajeros… (Nuestro equipo les va a situar cada servicio a su llegada por si fuese de su interés o lo necesitaran).</br>
                                        </br>
                                    </p>
                                </div> 
                            </div> 
                        </div> 
                        <div class="row"> 
                            <div class="col-md-offset-2 col-md-10 col-sm-offset-6 col-sm-6"> 
                                <ul data-wow-delay="0.2s" class="list-unstyled activities-list wow slideInRight" style="visibility: visible; animation-delay: 0.2s; animation-name: slideInRight;"> 
                                    <li> 
                                        <h4>Hotel Amazonas</h4> 
                                        <span>El hotel se encuentra cerca del Arenal, de bares y restaurantes. Cuenta con 155 habitaciones, piscina, campo de golf y terraza.</span>                                
                                    </li> 
                                    <li> 
                                        <h4>Sun Club El Dorado</h4> 
                                        <span>El Resort cuenta con 363 habitaciones y bungalows, 3 piscinas, pool bar, sala de juegos, pistas deportivas y animación nocturna.</span>                                
                                    </li> 
                                    <li> 
                                        <h4>Hotel Blue Sea</h4> 
                                        <span>El Hotel consta de 152 habitaciones, equipadas con aire acondicionado, TV, caja fuerte, además de un baño privado. </span>                                
                                    </li> 
                                    <li> 
                                        <h4>Hotel Costa Verde - CALIDAD-PRECIO</h4> 
                                        <span>El hotel se situa en el Arenal y consta de 124 habitaciones con aire acondicionado, TV, caja fuerte y balcón. </span>                                
                                    </li> 
                                    <li> 
                                        <h4>Hotel Fergus Géminis</h4> 
                                        <span>El hotel consta de piscinas, terraza, bar interior y diferentes planes de ocio y diversión.</span>
                                    </li> 
                                    <li> 
                                        <h4>Hotel Kilimanjaro</h4> 
                                        <span>El Hotel se encuentra en Lluchmajor y está equipado con todo lo necesario para que disfrutes de tus vacaciones en Mallorca.</span>
                                    </li>
                                    <li>  
                                        <h4>Hotel Luna Park - EL MÁS ESCOGIDO</h4> 
                                        <span>El Hotel se encuentra El Arenal, muy cerca del centro de Palma, y está equipado con todo lo necesario.</span>
                                    </li> 
                                    <li> 
                                        <h4>Hotel MLL Caribbean Bay</h4> 
                                        <span>El Hotel cuenta con un salón con áreas recreativas, dos piscinas y terraza superior.</span>
                                    </li> 
                                    <li> 
                                </ul> 
                            </div> 
                            <div class="col-md-offset-2 col-md-10 col-sm-offset-6 col-sm-6"> 
                                <ul data-wow-delay="0.3s" class="list-unstyled activities-list bordered wow slideInRight" style="visibility: visible; animation-delay: 0.3s; animation-name: slideInRight;"> 
                                    <li> 
                                        <h4>Hotel Palma Bay</h4> 
                                        <span>El Resort se situa en la playa de Palma, cerca de los restaurantes y bares. Consta de 4 zonas de piscina con jardines.</span>                                
                                    </li> 
                                    <li> 
                                        <h4>Hotel Paradise Music Beach</h4> 
                                        <span>Diversión, música, risas, amigos, sol, playa y mucha fiesta: estas son las vacaciones que te ofrece hotel.</span>
                                    </li> 
                                    <li> 
                                        <h4>Hotel Costa Mediterraneo</h4> 
                                        <span>El Hotel, situado en la zona turística de la playa de palma, encontrará todo tipo de servicios de ocio y diversión.</span>
                                    </li>
                                    <li> 
                                        <h4>Hotel Iris</h4> 
                                        <span>El Hotel Iris está situado a playa de Palma, dónde se puede encontrar restaurantes, cafés, pubs, etc.</span>                                
                                    </li> 
                                    <li> 
                                        <h4>Hotel Manaus - EL MÁS ECONÓMICO</h4> 
                                        <span>El Hotel está situado en una de las zonas más tranquilas de la playa de Palma.</span>
                                    </li> 
                                    <li> 
                                        <h4>Hotel Reina del Mar</h4> 
                                        <span>El Hotel se encuentra muy cerca de una conocida zona de ocio que cuenta con gran oferta y muy cerca de la playa El Arenal.</span>
                                    </li> 
                                    <li>  
                                        <h4>Hotel Mallorca Garden</h4> 
                                        <span>El hotel Mallorca Garden cuenta con una gran variedad de instalaciones, piscina, restaurante y bar-terraza</span>
                                    </li> 
                                    <li> 
                                        <h4>Hotel Riutort</h4> 
                                        <span>Hotel situado en la zona del Arenal, donde encontrará todo tipo de servicios, deportes, tiendas y locales de diversión.</span>
                                    </li> 
                                    <li>
                                        <h4>Hotel Whala Beach - EL MEJOR SITUADO</h4> 
                                        <span>Hotel cálido y acogedor en Playa de Palma. Cuenta con todo tipo de servicios e instalaciones para disfrutar de tu estancia. </span>
                                    </li> 
                                    <li> 
                                </ul> 
                            </div> <!-- /.trip activities --> 
                        </div>
                    </div>
                </div> 
            </div> 
        </section> 

        
        <section id="trip-discotecas" style="margin:85px 0"> 
            <div class="container" style="border-bottom: 10px solid #f2f0ec; padding-bottom:30px"> <!-- section-intro --> 
                <div class="row text-center section-intro">
                    <div class="col-md-12"> 
                        <i data-wow-delay="0.2s" class="icon icon-wine wow  fadeInUp animated" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;"></i> 
                        <h1 data-wow-delay="0.4s" class="text-uppercase wow  fadeInUp animated" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
                            Discotecas (opcionales) +16 años
                        </h1> 
                        <p data-wow-delay="0.4s" class="wow  fadeInUp animated" style="margin: 0px; visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
                            Colaboramos el Grupo GCO, grupo Cursach, propietario de las discotecas BCM, Titos, Pacha… y te ponemos en contacto con los comerciales oficiales del Grupo, que te atenderán y explicarán los paquetes que tienen y cómo funcionan. Te acompañarán nuestros asistentes/coordinadores tanto a la ida como a la vuelta, que incluye transporte, copa/refresco y/o regalo…</br></br>
                            Bono con todas las discotecas 100€ (Tito's, Pacha, Gothic, Aqualand, Millenium, BCM)</br>
							Bono con todas las discotecas sin BCM 85€ (Tito's, Pacha, Gothic, Aqualand, Millenium)

                        </p>
                    </div> 
                </div> <!-- /.section-intro --> <!-- place tabs --> 
                
                <div class="row"> 
                    <div class="col-md-12"> <!-- Nav tabs --> 
                        <ul role="tablist" class="nav nav-tabs places-tabs text-uppercase text-center"> 
                            <li class="active wow pulse" role="presentation"> 
                                <a data-toggle="tab" role="tab" aria-controls="day1-tab" href="#day1-tab">
                                    <?= $d->row(0)->nombre ?>
                                </a>
                            </li> 
                            <li  class="wow pulse" role="presentation">
                                <a data-toggle="tab" role="tab" aria-controls="day2-tab" href="#day2-tab">
                                   <?= $d->row(1)->nombre ?>
                                </a>
                            </li> 
                            <li  class="wow pulse" role="presentation"> 
                                <a data-toggle="tab" role="tab" aria-controls="day3-tab" href="#day3-tab">
                                    <?= $d->row(2)->nombre ?>
                                </a>
                            </li> 
                            <li class="wow pulse" role="presentation"> 
                                <a data-toggle="tab" role="tab" aria-controls="day4-tab" href="#day4-tab">
                                    <?= $d->row(3)->nombre ?>
                                </a>
                            </li> 
                            <li  class="wow pulse" role="presentation"> 
                                <a data-toggle="tab" role="tab" aria-controls="day5-tab" href="#day5-tab">
                                    <?= $d->row(4)->nombre ?>
                                </a>
                            </li> 
                            <li class="wow pulse" role="presentation"> 
                                <a data-toggle="tab" role="tab" aria-controls="day6-tab" href="#day6-tab">
                                    <?= $d->row(5)->nombre ?>
                                </a>
                            </li> 
                        </ul> <!-- Tab panes --> 
                        <div class="tab-content places-content" style="border-bottom: 0px solid #f2f0ec; margin-bottom: 0px"> 
                            <div id="day1-tab" class="tab-pane active" role="tabpanel"> 
                                <div class="row"> 
                                    <div class="col-md-4"> 
                                        <div id="road-map" class="route-map"></div>
                                        <?= img('images/discotecas/'.$d->row(0)->logo,'width:100%') ?> 
                                    </div>
                                    <div class="col-md-4"> 
                                        <div class="day-description"> 
                                            <h4 class="text-capitalize"><?= $d->row(0)->titulo ?></h4> 
                                            <p style="margin: 0px;"> <?= $d->row(0)->subtitulo ?>  </p>
                                            <span> <?= $d->row(0)->descripcion ?>  </span>
                                        </div>
                                    </div>
                                    <div class="col-md-4"> 
                                        <ul class="list-unstyled activities-list bordered"> 
                                            <li> 
                                                <h4>PRECIOS INDIVIDUALES Y BONOS</h4> 
                                                <span> <?= $d->row(0)->precios_individuales ?>  </span>
                                            </li> 
                                            <li> 
                                                <h4>El precio del bono incluye:</h4> 
                                                <span> <?= $d->row(0)->precios_bono ?>  </span>                                                
                                            </li> 
                                            <li> 
                                                <h4>ATENCIÓN</h4> 
                                                <span> <?= $d->row(0)->atencion ?>  </span>              
                                            </li> 
                                        </ul> 
                                    </div> 
                                    <?= img('images/discotecas/'.$d->row(0)->banner,'width:100%') ?> 
                                </div> 
                            </div> 
                            <div id="day2-tab" class="tab-pane" role="tabpanel"> 
                                <div class="row"> 
                                    <div class="col-md-4"> 
                                        <div id="road-map2" class="route-map"></div>
                                         <?= img('images/discotecas/'.$d->row(1)->logo,'width:100%') ?> 
                                    </div> 
                                    <div class="col-md-4"> 
                                        <div class="day-description"> 
                                            <h4 class="text-capitalize"><?= $d->row(1)->titulo ?></h4> 
                                            <p style="margin: 0px;"> <?= $d->row(1)->subtitulo ?>  </p>
                                            <span> <?= $d->row(1)->descripcion ?>  </span>
                                        </div> 
                                    </div> 
                                    <div class="col-md-4"> 
                                        <ul class="list-unstyled activities-list bordered"> 
                                            <li> 
                                                <h4>PRECIOS INDIVIDUALES Y BONOS</h4> 
                                                <span> <?= $d->row(1)->precios_individuales ?>  </span>
                                            </li> 
                                            <li> 
                                                <h4>El precio del bono incluye:</h4> 
                                                <span> <?= $d->row(1)->precios_bono ?>  </span>                                                
                                            </li> 
                                            <li> 
                                                <h4>ATENCIÓN</h4> 
                                                <span> <?= $d->row(1)->atencion ?>  </span>              
                                            </li> 
                                        </ul> 
                                    </div>
                                     <?= img('images/discotecas/'.$d->row(1)->banner,'width:100%') ?> 
                                </div> 
                            </div>
                            <div id="day3-tab" class="tab-pane" role="tabpanel"> <div class="row"> 
                                    <div class="col-md-4"> 
                                        <div id="road-map3" class="route-map"></div> 
                                        <?= img('images/discotecas/'.$d->row(2)->logo,'width:100%') ?> 
                                    </div> 
                                    <div class="col-md-4"> 
                                        <div class="day-description"> 
                                            <h4 class="text-capitalize"><?= $d->row(2)->titulo ?></h4> 
                                            <p style="margin: 0px;"> <?= $d->row(2)->subtitulo ?>  </p>                                            
                                            <span> <?= $d->row(2)->descripcion ?>  </span>
                                        </div> 
                                    </div> 
                                    <div class="col-md-4"> 
                                        <ul class="list-unstyled activities-list bordered"> 
                                            <li> 
                                                <h4>PRECIOS INDIVIDUALES Y BONOS</h4> 
                                                <span> <?= $d->row(2)->precios_individuales ?>  </span>
                                            </li> 
                                            <li> 
                                                <h4>El precio del bono incluye:</h4> 
                                                <span> <?= $d->row(2)->precios_bono ?>  </span>
                                            </li> 
                                            <li> 
                                                <h4>ATENCIÓN</h4> 
                                                <span> <?= $d->row(2)->atencion ?>  </span>              
                                            </li> 
                                        </ul> 
                                    </div>
                                    <?= img('images/discotecas/'.$d->row(2)->banner,'width:100%') ?> 
                                </div>
                            </div>
                        <div id="day4-tab" class="tab-pane" role="tabpanel"> <div class="row"> 
                                    <div class="col-md-4"> 
                                        <div id="road-map4" class="route-map"></div> 
                                        <?= img('images/discotecas/'.$d->row(3)->logo,'width:100%') ?> 
                                    </div> 
                                    <div class="col-md-4"> 
                                        <div class="day-description"> 
                                            <h4 class="text-capitalize"><?= $d->row(3)->titulo ?></h4> 
                                            <p style="margin: 0px;"> <?= $d->row(3)->subtitulo ?>  </p>
                                            <span> <?= $d->row(3)->descripcion ?>  </span>
                                        </div> 
                                    </div> 
                                    <div class="col-md-4"> 
                                        <ul class="list-unstyled activities-list bordered"> 
                                            <li> 
                                                <h4>PRECIOS INDIVIDUALES Y BONOS</h4> 
                                                <span> <?= $d->row(3)->precios_individuales ?>  </span>
                                            </li> 
                                            <li> 
                                                <h4>El precio del bono incluye:</h4> 
                                                <span> <?= $d->row(3)->precios_bono ?>  </span>                                                
                                            </li> 
                                            <li> 
                                                <h4>ATENCIÓN</h4> 
                                                <span> <?= $d->row(3)->atencion ?>  </span>              
                                            </li> 
                                        </ul> 
                                    </div>
                                    <?= img('images/discotecas/'.$d->row(3)->banner,'width:100%') ?>    
                                </div>
                                </div>
                                <div id="day5-tab" class="tab-pane" role="tabpanel"> <div class="row"> 
                                    <div class="col-md-4"> 
                                        <div id="road-map5" class="route-map"></div> 
                                        <?= img('images/discotecas/'.$d->row(4)->logo,'width:100%') ?> 
                                    </div> 
                                    <div class="col-md-4"> 
                                        <div class="day-description"> 
                                            <h4 class="text-capitalize"><?= $d->row(4)->titulo ?></h4> 
                                            <p style="margin: 0px;"> <?= $d->row(4)->subtitulo ?>  </p>                                            
                                            <span> <?= $d->row(4)->descripcion ?>  </span>
                                        </div> 
                                    </div> 
                                    <div class="col-md-4"> 
                                        <ul class="list-unstyled activities-list bordered"> 
                                            <li> 
                                                <h4>PRECIOS INDIVIDUALES Y BONOS</h4> 
                                                <span> <?= $d->row(4)->precios_individuales ?>  </span> 
                                            </li> 
                                            <li> 
                                                <h4>El precio del bono incluye:</h4> 
                                                <span> <?= $d->row(4)->precios_bono ?>  </span>
                                            </li> 
                                            <li> 
                                                <h4>ATENCIÓN</h4> 
                                                <span> <?= $d->row(4)->atencion ?>  </span>              
                                            </li> 
                                        </ul> 
                                    </div>
                                    <?= img('images/discotecas/'.$d->row(4)->banner,'width:100%') ?> 
                                </div>
                    </div>
                    <div id="day6-tab" class="tab-pane" role="tabpanel"> <div class="row"> 
                                    <div class="col-md-4"> 
                                        <div id="road-map6" class="route-map"></div> 
                                        <?= img('images/discotecas/'.$d->row(5)->logo,'width:100%') ?> 
                                    </div> 
                                    <div class="col-md-4"> 
                                        <div class="day-description"> 
                                            <h4 class="text-capitalize"><?= $d->row(5)->titulo ?></h4> 
                                            <p style="margin: 0px;"> <?= $d->row(5)->subtitulo ?>  </p>                                            
                                            <span> <?= $d->row(5)->descripcion ?>  </span>
                                        </div> 
                                    </div> 
                                    <div class="col-md-4"> 
                                        <ul class="list-unstyled activities-list bordered"> 
                                            <li> 
                                                <h4>PRECIOS INDIVIDUALES Y BONOS</h4> 
                                                <span> <?= $d->row(5)->precios_individuales ?>  </span>
                                            </li> 
                                            <li> 
                                                <h4>El precio del bono incluye:</h4> 
                                                <span> <?= $d->row(5)->precios_bono ?>  </span>                                                
                                            </li> 
                                            <li> 
                                                <h4>ATENCIÓN</h4> 
                                                <span> <?= $d->row(5)->atencion ?>  </span>              
                                            </li> 
                                        </ul> 
                                    </div>
                                    <?= img('images/discotecas/'.$d->row(5)->banner,'width:100%') ?> 
                                </div>
                </div>
                        </div>
            </div>
        </section>
        <section style="margin-top:-85px">
            <iframe width="100%" height="315" src="https://www.youtube.com/embed/dpdW13frPx8" frameborder="0" allowfullscreen></iframe>
        </section>
        


        
        <section id="trip-seguros" style="margin:85px 0;"> <!-- trip activities background --> 
            <div data-bg="<?= base_url() ?>images/avion.jpg" class="trip-activities-bg wow slideInLeft" style="background-image: url(<?= base_url() ?>images/avion.jpg); visibility: visible; animation-name: slideInLeft;">
                
            </div> <!-- /.trip activities background --> 
            <div class="container"> 
                <div class="row">
                    <div class="col-sm-4 col-md-6">
                        
                    </div>
                    <div class="col-sm-8 col-md-6">
                        <div class="row"> <!-- trip activities --> 
                            <div class="col-md-offset-2 col-md-10 col-sm-offset-6 col-sm-6"> 
                                <div class="section-intro"> 
                                    <i data-wow-delay="0.4s" class="icon icon-pencil wow fadeInLeft" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInLeft;"></i> 
                                    <h1 data-wow-delay="0.5s" class="text-uppercase wow fadeInLeft" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInLeft;">
                                        Seguros
                                    </h1> 
                                    <p data-wow-delay="0.6s" class="wow fadeInLeft" style="margin: 0px; visibility: visible; animation-delay: 0.6s; animation-name: fadeInLeft;">
                                        Tener un buen seguro es síntoma de tranquilidad. Por ello trabajamos con las compañías aseguradoras líderes en el sector colectivo de estudiantes. En este punto le confirmamos que en el viaje está incluido el seguro obligatorio básico/estándar de asistencia médica, sanitaria y RC, con anulación por causas justificadas, incluso por suspensos (según condicionado de la póliza).</p>
                                </div> 
                            </div> 
                        </div> 
                        <div class="row"> 
                            <div class="col-md-offset-2 col-md-10 col-sm-offset-6 col-sm-6"> 
                                <ul data-wow-delay="0.2s" class="list-unstyled activities-list wow slideInRight" style="visibility: visible; animation-delay: 0.2s; animation-name: slideInRight;"> 
                                    <li> 
                                        <h4>Asistencia médica y sanitaria</h4> 
                                        <span>Se proporcionará al Asegurado un conjunto de servicios con el fin de promover, proteger y restaurar su salud, acorde con la cobertura sanitaria contratada.</span>                                
                                    </li> 
                                    <li> 
                                        <h4>Seguros de Responsabilidad Civil</h4> 
                                        <span>Éstos seguros son creados para reparar el daño causado a otro con el que no existía un vínculo previo, mediante el pago de una indemnización de perjuicios.</span>                                
                                    </li> 
                                    <li> 
                                        <h4>AXA ASSISTANCE ESTUDIANTES</h4> 
                                        <span>Axa Assistance Estudiantes es el seguro que se ofrece incluido con la compra del paquete. A continuación, podréis ver cuáles son los aspectos básicos/esenciales que cubre este seguro.</span>                                
                                    </li> 
                                </ul> 
                            </div> 
                            <div class="col-md-offset-2 col-md-10 col-sm-offset-6 col-sm-6"> 
                                <ul data-wow-delay="0.3s" class="list-unstyled activities-list bordered wow slideInRight" style="visibility: visible; animation-delay: 0.3s; animation-name: slideInRight;"> 
                                    <li> 
                                        <h4>HAZ TU SEGURO A LO GRANDE</h4> 
                                        <span>Trabajamos con las compañías aseguradoras líderes en el sector de colectivos de estudiantes, para ello os ofrecemos también la posibilidad de aumentar la cobertura de asistencia médica y sanitaria durante el viaje, seguro para los equipajes, gastos de anulación, accidentes y RC, por tan sólo 21€ más por persona.
                                            </br></br>A continuación, podréis consultar el resumen completo de coberturas y límites con la ampliación Multiassistance Plus Élite.</span>                                
                                    </li> 

                                </ul> 
                            </div> <!-- /.trip activities --> 
                        </div> 
                    </div>
                </div>
            </div> 
        </section>




        <?php 

            $seguro1 = $this->db->get_where('paginas',array('id'=>5))->row(); 
            $seguro2 = $this->db->get_where('paginas',array('id'=>6))->row(); 

        ?>

        <section data-bg="<?= base_url() ?>images/charge-bg.jpg" class="trip-charge trip-charge2" style="background-image: url(<?= base_url() ?>images/charge-bg.jpg);"> 
            <div class="container"> 
                <div id="price" class="row"> 
                    <div class="col-md-3"> 
                        <i class="icon icon-tools"></i> 
                        <h1 class="text-uppercase">SEGURO A LO GRANDE</h1> 
                    </div> 
                    <div class="col-md-offset-1 col-md-8"> 
                        <div class="price-block"> 
                            <h1> 
                                <?= $seguro1->precio ?>
                            </h1> 
                            <span>por persona</span>                            
                        </div> 
                        <div class="price-description"> 
                            <p style="margin: 0px; font-size: 24px;">
                                <?= $seguro1->titulo ?> </br>
                               <p style="font-size:15px"><?= $seguro1->subtitulo ?></p>
                        </div> 
                    </div> 
                </div> 
                <div class="row"> 
                    <div class="col-xs-12" style="background: rgba(242, 240, 236, 1) none repeat scroll 0% 0%; margin: 50px auto; min-height: 300px; padding:20px; font-family: serif">
                        <?= $seguro1->contenido ?>
                    </div>
                </div> 
            </div>                         
        </section>
        
        <section data-bg="<?= base_url() ?>images/charge-bg.jpg" class="trip-charge trip-charge2" style="background-image: url(<?= base_url() ?>images/charge-bg.jpg); padding-top:0"> 
            <div class="container"> 
                <div id="price" class="row"> 
                    <div class="col-md-offset-4 col-md-8"> 
                        <div class="price-block"> 
                            <h1> 
                                <?= $seguro2->precio ?>
                            </h1> 
                            <span>por persona</span>                            
                        </div> 
                        <div class="price-description"> 
                            <p style="margin: 0px; font-size: 24px;"><?= $seguro2->titulo ?></p> <p style="font-size:15px"><?= $seguro2->subtitulo ?> </p>
                          
                        </div> 
                    </div> 
                </div> 
                <div class="row"> 
                    <div class="col-xs-12" style="background: rgba(242, 240, 236, 1) none repeat scroll 0% 0%; margin: 50px auto; min-height: 300px; padding:20px; font-family: serif">
                        <?= $seguro2->contenido ?>
                    </div>
                </div> 
            </div>                         
        </section>
        
       
        <section id="trip-material-viaja-gratis" style="margin:85px 0;"> <!-- trip activities background --> 
            <div class="trip-activities-bg wow slideInLeft" style="background-image: url(<?= base_url() ?>images/gratis.jpg); visibility: visible; animation-name: slideInLeft;">
                
            </div> <!-- /.trip activities background --> 
            <div class="container"> 
                <div class="col-sm-4 col-md-6">
                    
                </div>
                <div class="col-sm-8 col-md-6">
                    <div class="row"> <!-- trip activities --> 
                        <div class="col-md-offset-2 col-md-10 col-sm-offset-6 col-sm-6"> 
                            <div class="section-intro"> 
                                <i data-wow-delay="0.4s" class="icon icon-basket wow fadeInLeft" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInLeft;"></i> 
                                <h1 data-wow-delay="0.5s" class="text-uppercase wow fadeInLeft" style="visibility: visible; animation-delay: 0.5s; animation-name: fadeInLeft;">
                                    Material Viaja Gratis
                                </h1> 
                                <p data-wow-delay="0.6s" class="wow fadeInLeft" style="margin: 0px; visibility: visible; animation-delay: 0.6s; animation-name: fadeInLeft;">
                                    Consigue tu viaje por 0€.
                                </p>
                            </div> 
                        </div> 
                    </div> 
                    <div class="row"> 
                        <div class="col-md-offset-2 col-md-10 col-sm-offset-6 col-sm-6"> 
                            <ul data-wow-delay="0.2s" class="list-unstyled activities-list wow slideInRight" style="visibility: visible; animation-delay: 0.2s; animation-name: slideInRight;"> 
                                <li> 
                                    <h4>Vende papeletas</h4> 
                                    <span>Al reservar el viaje se da a cada viajero papeletas por valor de 300€, 
                                        que son para el propio viajero, para ayudarle a su autofinanciación. 
                                        El Viajero debe entregar en la fecha que se determine la matriz de las mismas para su control, 
                                        y en caso de cancelación o desistimiento del viaje deberá devolver las papeletas de forma íntegra 
                                        y/o el importe que haya sacado con las mismas ya que son única y exclusivamente de autofinanciación.</span>                                

                                </li>
                                <li>  
                                    <h4>Merchandising</h4> 
                                    <span>Siempre que el/los viajer@s lo soliciten se podrán dar prendas para que puedan venderlas y de esta forma que sirva de autofinanciación.</br>
                                        Sudaderas, camisetas, gorras…. </span>                    
                                </li> 
                                <li> 
                                    <h4>Ver  y comprar Merchandising</h4> 
                                    <span>Aquí puedes ver todos nuestros artículos y hacer tu pedido. 

                                    </span></br>
                                    </br>
                                    <a href="mailto:info@miex.me?Subject=Hello%20again" target="_top">SOLICITAR MERCHANDANSING</a>
                                </li>           
                            </ul> 
                        </div> 
                        <div class="col-md-offset-2 col-md-10 col-sm-offset-6 col-sm-6"> 
                            <ul data-wow-delay="0.3s" class="list-unstyled activities-list bordered wow slideInRight" style="visibility: visible; animation-delay: 0.3s; animation-name: slideInRight;"> 
                                <li> 

                                </li> 
                                <li> 


                            </ul> 
                            <div class="vc_col-md-6"> 
                                <ul class="list-inline gallery-photos"> 
                                    <li> 
                                        <a href="http://miex.me/images/m1.jpg">
                                            <img class="img-responsive" alt="photo-gallery" src="http://miex.me/images/m1.jpg">
                                        </a>
                                    </li>
                                    <li> 
                                        <a href="http://miex.me/images/m2.jpg">
                                            <img class="img-responsive" alt="photo-gallery" src="http://miex.me/images//m2.jpg">
                                        </a>
                                    </li>
                                    <li> 
                                        <a href="http://miex.me/images/m3.jpg">
                                            <img class="img-responsive" alt="photo-gallery" src="http://miex.me/images//m3.jpg">
                                        </a>
                                    </li>
                                    <li> 
                                        <a href="http://miex.me/images/m4.jpg">
                                            <img class="img-responsive" alt="photo-gallery" src="http://miex.me/images/m4.jpg">
                                        </a>
                                    </li>
                                    <li> 
                                        <a href="http://miex.me/images/m5.jpg">
                                            <img class="img-responsive" alt="photo-gallery" src="http://miex.me/images/m5.jpg">
                                        </a>
                                    </li>

                                </ul> 
                            </div>

                        </div> 
                    </div>
                </div>
            </div>
                    
        </section>
        <section id="trip-autorizacion-paterna" class="adventure-select"> 
            <div class="container"> 
                <div class="row"> <!-- adventure list --> 
                    <div class="text-uppercase adventure-list experience"> 
                        <div data-wow-duration="1s" data-wow-delay="0.1s" class="col-md-12 col-sm-12 animated fadeInUp"> 
                            <a href="#"> 
                                <img class="img-responsive" alt="adventure-image" src="<?= base_url() ?>images/paternas.jpg"> 
                                <div class="overlay-lnk text-uppercase text-center">
                                <i class="icon icon-paperclip"></i>                                      
                                    <h5>CONDICIONES GENERALES</h5> 
                                </div> 
                            </a>
                        </div>
                    </div> <!-- /.adventure list --> 
                </div> 
            </div> 
        </section>
        <section id="trip-autorizacion-paterna-content" class="adventure-select" style="font-size:15px; font-family: PT serif">
            <div class="container"> 
                <div class="row"> <!-- adventure list --> 
                    <?= $this->db->get_where('paginas',array('id'=>12))->row()->contenido ?>
                </div> 
                
                <section id="trip-discotecas" style="margin:0px 0"> 
            <div class="container" style="border-bottom: 10px solid #f2f0ec; padding-bottom:30px"> <!-- section-intro --> 
                <div class="row text-center section-intro">
                    <div class="col-md-12"> 
                                                    <i class="icon icon-profile-male"></i>  <i class="icon icon-profile-female"></i>                                      
                        <h1 data-wow-delay="0.4s" class="text-uppercase wow  fadeInUp animated" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp; font-family: Raleway">
                            INFORMACION PARA LOS PADRES
                        </h1>
                   
        		</section>
                            </li> 
                           
                              
                            
                        
                        
                <div class="row" style="margin-top: 30px"> <!-- adventure list --> 
                    <?= $this->db->get_where('paginas',array('id'=>13))->row()->contenido ?>
                </div> 
        </section>
        <section id="trip-preguntas-frecuentes" style=" padding-bottom: 70px">
            <div class="container"> 
                <div class="row"> <!-- adventure list --> 
                    <div class="text-uppercase adventure-list experience"> 
                        <div data-wow-duration="1s" data-wow-delay="0.1s" class="col-md-12 col-sm-12 animated fadeInUp"> 
                            <a href="#"> 
                                <img class="img-responsive" alt="adventure-image" src="<?= base_url() ?>images/preguntas.jpg"> 
                                <div class="overlay-lnk text-uppercase text-center">                                     
                                <i class="icon icon-chat"></i> 
                                <h5>PREGUNTAS FRECUENTES</h5> 
                                </div> 
                            </a>
                        </div>
                    </div> <!-- /.adventure list --> 
                </div> 
            </div> 
        </section>
        <section class="adventure-select" id="trip-preguntas-frecuentes-content" style="margin-bottom: px; font-size:15px; font-family: PT serif">
            <div class="container"> 
                <div class="row"> <!-- adventure list --> 
                    <?= $this->db->get_where('paginas',array('id'=>14))->row()->contenido ?>
                </div> 
            </div> 
        </section>
         <?= $this->load->view('includes/template/contact') ?>
    <button class="btn goUp-btn"> 
        <i class="fa fa-angle-up"></i> 
        <span>SUBIR</span>
        <span class="fitz">noninfantry</span> 
    </button> <!-- /.go up arrow -->
</main>
<?= $this->load->view('includes/template/footer'); ?>
</div>
</div> <!-- /.page content --> <!-- plugins we use --> 
<script src="js/functions.js"></script> <!-- snazzy maps --> 
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAVZtQkD2-J7WPeAGKw1U9akvqZ1IsEKvA" type="text/javascript"></script>
    <script> 
      google.maps.event.addDomListener(window, 'load', initMap3);
      var markericon = '<?= base_url() ?>images/map-locator.png';
      var map1, map2, map3;
      function initMap3() {
          var mapOptions1 = {
              center: new google.maps.LatLng<?= $d->row(0)->coordenadas ?> ,
              zoom: 15,
              zoomControl: true,
              disableDoubleClickZoom: true,
              mapTypeControl: false,
              scaleControl: false,
              scrollwheel: false,
              panControl: false,
              streetViewControl: false,
              draggable : true,
              overviewMapControl: false,
              overviewMapControlOptions: {
                  opened: false,
              },
              mapTypeId: google.maps.MapTypeId.ROADMAP,
              styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}],
          };
          var mapElement1 = document.getElementById('road-map');
          map1 = new google.maps.Map(mapElement1, mapOptions1);
          if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {            
            map1.setOptions({ 'draggable': false });
        }
          marker = new google.maps.Marker({
            icon: markericon,
            position: new google.maps.LatLng<?= $d->row(0)->coordenadas ?> ,
            map: map1
          });
      }
      
      function initMap4() {
          var mapOptions1 = {
              center: new google.maps.LatLng<?= $d->row(1)->coordenadas ?> ,
              zoom: 15,
              zoomControl: true,
              disableDoubleClickZoom: true,
              mapTypeControl: false,
              scaleControl: false,
              scrollwheel: false,
              panControl: false,
              streetViewControl: false,
              draggable : true,
              overviewMapControl: false,
              overviewMapControlOptions: {
                  opened: false,
              },
              mapTypeId: google.maps.MapTypeId.ROADMAP,
              styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}],
          };
          var mapElement1 = document.getElementById('road-map2');
          map2 = new google.maps.Map(mapElement1, mapOptions1);
          if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {            
                map2.setOptions({ 'draggable': false });
            }
          marker = new google.maps.Marker({
            icon: markericon,
            position: new google.maps.LatLng<?= $d->row(1)->coordenadas ?> ,
            map: map2
          });
     
      }
      function initMap5() {
          var mapOptions1 = {
              center: new google.maps.LatLng<?= $d->row(2)->coordenadas ?> ,
              zoom: 15,
              zoomControl: true,
              disableDoubleClickZoom: true,
              mapTypeControl: false,
              scaleControl: false,
              scrollwheel: false,
              panControl: false,
              streetViewControl: false,
              draggable : true,
              overviewMapControl: false,
              overviewMapControlOptions: {
                  opened: false,
              },
              mapTypeId: google.maps.MapTypeId.ROADMAP,
              styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}],
          };
          var mapElement1 = document.getElementById('road-map3');
          map3 = new google.maps.Map(mapElement1, mapOptions1);
          if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            map3.setOptions({ 'draggable': false });
        }
          marker = new google.maps.Marker({
            icon: markericon,
            position: new google.maps.LatLng<?= $d->row(2)->coordenadas ?> ,
            map: map3
          });     
      }
      
      function initMap6() {
          var mapOptions1 = {
              center: new google.maps.LatLng<?= $d->row(3)->coordenadas ?> ,
              zoom: 15,
              zoomControl: true,
              disableDoubleClickZoom: true,
              mapTypeControl: false,
              scaleControl: false,
              scrollwheel: false,
              panControl: false,
              streetViewControl: false,
              draggable : true,
              overviewMapControl: false,
              overviewMapControlOptions: {
                  opened: false,
              },
              mapTypeId: google.maps.MapTypeId.ROADMAP,
              styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}],
          };
          var mapElement1 = document.getElementById('road-map4');
          map4 = new google.maps.Map(mapElement1, mapOptions1);
          if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            map4.setOptions({ 'draggable': false });
        }
          marker = new google.maps.Marker({
            icon: markericon,
            position: new google.maps.LatLng<?= $d->row(3)->coordenadas ?> ,
            map: map4
          });     
      }
      
      function initMap7() {
          var mapOptions1 = {
              center: new google.maps.LatLng<?= $d->row(4)->coordenadas ?> ,
              zoom: 15,
              zoomControl: true,
              disableDoubleClickZoom: true,
              mapTypeControl: false,
              scaleControl: false,
              scrollwheel: false,
              panControl: false,
              streetViewControl: false,
              draggable : true,
              overviewMapControl: false,
              overviewMapControlOptions: {
                  opened: false,
              },
              mapTypeId: google.maps.MapTypeId.ROADMAP,
              styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}],
          };
          var mapElement1 = document.getElementById('road-map5');
          map5 = new google.maps.Map(mapElement1, mapOptions1);
          if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            map5.setOptions({ 'draggable': false });
        }
          marker = new google.maps.Marker({
            icon: markericon,
            position: new google.maps.LatLng<?= $d->row(4)->coordenadas ?> ,
            map: map5
          });     
      }
      
      function initMap8() {
          var mapOptions1 = {
              center: new google.maps.LatLng<?= $d->row(5)->coordenadas ?> ,
              zoom: 15,
              zoomControl: true,
              disableDoubleClickZoom: true,
              mapTypeControl: false,
              scaleControl: false,
              scrollwheel: false,
              panControl: false,
              streetViewControl: false,
              draggable : true,
              overviewMapControl: false,
              overviewMapControlOptions: {
                  opened: false,
              },
              mapTypeId: google.maps.MapTypeId.ROADMAP,
              styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#fefefe"},{"lightness":20}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#fefefe"},{"lightness":17},{"weight":1.2}]}],
          };
          var mapElement1 = document.getElementById('road-map6');
          map6 = new google.maps.Map(mapElement1, mapOptions1);
          if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            map6.setOptions({ 'draggable': false });
        }
          marker = new google.maps.Marker({
            icon: markericon,
            position: new google.maps.LatLng<?= $d->row(5)->coordenadas ?> ,
            map: map6
          });     
      }
    </script>
    <script src="<?= base_url() ?>js/main.js"></script>
</body>
</html>