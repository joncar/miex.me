<div class="about-page loaded" id="page-content">
<header data-bg="<?= base_url() ?>images/about_header_bg.jpg" class="overlay" style="background-image: url(<?= base_url() ?>images/about_header_bg.jpg);">
    <?= $this->load->view('includes/template/menu2') ?>
    <div class="header-center-content"> 
        <div class="container text-center"> 
            <div class="row"> 
                <div class="col-md-offset-2 col-md-8 animated fadeInUp"> 
                <i class="icon icon-target"style=" font-size: 60px; color: #f71259;"></i>
                    <h1 class="text-uppercase">NUESTRO TRABAJO ES HACERLO BIEN</h1> 
                    <h4>La experiencia es nuestra mejor presentación</h4> 
                </div> 
            </div> 
        </div> 
    </div> 
</header> <!-- /.about page header --> <!-- main content --> 
<main> 
    <section class="about-tabs"> <!-- Tab panes --> 
        <div class="container-fluid"> 
            <div class="row about-tabs-control"> 
                <div class="container"> <!-- Nav tabs --> 
                    <ul role="tablist" class="nav nav-tabs"> 
                        <li class="active" role="presentation">
                            <a data-toggle="tab" role="tab" aria-controls="mission" href="#mission">Equipo</a>
                        </li> 
                        <li role="presentation">
                            <a data-toggle="tab" role="tab" aria-controls="values" href="#values">Valores</a>
                        </li> 
                        <li role="presentation">
                            <a data-toggle="tab" role="tab" aria-controls="experience" href="#experience">Experiencia</a>
                        </li> 
                    </ul> <!-- /.Nav tabs --> 
                </div>
            </div> 
            <div class="row"> 
                <div class="tab-content"> 
                    <div id="mission" class="tab-pane fade in active" role="tabpanel"> 
                        <div class="col-md-6 noPaddingLeft"> 
                            <div data-bg="<?= base_url() ?>images/mission_bg.jpg" class="text-uppercase tab-bg" style=" background-image: url(http://miex.me/images/mission_bg.jpg); /* padding-top: */ margin-top: 19px"> 
                                <h1 class="tab-title">Nuestro equipo</h1> 
                            </div> 
                        </div> 
                        <div class="col-md-6"> 
                            <div class="tab-wrapp"> 
                                <?= $this->db->get_where('paginas',array('id'=>7))->row()->contenido;  ?>
                            </div> 
                        </div> 
                    </div> 
                    <div id="values" class="tab-pane fade" role="tabpanel"> 
                        <div class="col-md-6 noPaddingLeft"> 
                            <div data-bg="<?= base_url() ?>images/values_bg.jpg" class="text-uppercase tab-bg" style=" background-image: url(http://miex.me/images/mission_bg.jpg); /* padding-top: */ margin-top: 19px"> 
                                <h1 class="tab-title">Nuestros valores</h1> 
                            </div> 
                        </div> 
                        <div class="col-md-6"> 
                            <div class="tab-wrapp"> 
                                <?= $this->db->get_where('paginas',array('id'=>8))->row()->contenido;  ?>
                            </div> 
                        </div> 
                    </div> 
                    <div id="experience" class="tab-pane fade" role="tabpanel"> 
                        <div class="col-md-6 noPaddingLeft"> 
                            <div data-bg="<?= base_url() ?>images/experience_bg.jpg" class="text-uppercase tab-bg" style=" background-image: url(http://miex.me/images/mission_bg.jpg); /* padding-top: */ margin-top: 19px"> 
                                <h1 class="tab-title">Nuestra experiencia</h1> 
                            </div> 
                        </div> 
                        <div class="col-md-6"> 
                            <div class="tab-wrapp"> 
                                <?= $this->db->get_where('paginas',array('id'=>9))->row()->contenido;  ?>
                            </div> 
                        </div> 
                    </div> 
                </div> 
            </div> 
        </div> <!-- /.Tab panes --> <!-- trip categories --> 
        <div class="row trip-categories"> 
            <div class="container"> <!-- section-intro --> 
                <div class="row text-center section-intro"> 
                    <div class="col-md-offset-2 col-md-8">
                            <h1 class="text-uppercase wow animated fadeInUp" style="animation-delay: 0.4s; animation-name: none;">
                                TE LO QUEREMOS PONER FÁCIL
                            </h1> 
                            <p style="margin: 0px;">Tanto es así, que Mallorca Island Experience, nos ocuparemos de todo</p>
                    </div> 
                </div> <!-- /.section-intro --> 
                <div class="row"> 
                    <div class="col-md-3 col-sm-6 text-center"> 
                        <div class="category"> 
                            <i class="icon icon-search"></i> 
                            <h5 class="text-uppercase category-title"> 
                                Información de reserva de vuestro viaje
                            </h5> 
                            <p style="margin: 0px;"> 
                                Información de la reserva de vuestro viaje, desde la propia web, por medio de los asistentes, en nuestra oficina, teléfonos de contacto, y mail. 
                            </p>
                            <a class="btn text-uppercase" href="<?= base_url() ?>p/actividades#trip-actividades-incluidas">Ver más</a>
                        </div> 
                    </div> 
                    <div class="col-md-3 col-sm-6 text-center"> 
                        <div class="category"> 
                            <i class="icon icon-key"></i> 
                            <h5 class="text-uppercase category-title">
                                Gestión de Rooming-List / Check-in & Check-out
                            </h5> 
                            <p style="margin: 0px;"> 
                                Gestión del room list y gestiones con el hotel,  confección de la room list, auxilio y asesoramiento por parte de nuestros asistentes al grupo en el check in y check out. 
                            </p>
                            <a class="btn text-uppercase" href="<?= base_url() ?>p/actividades#trip-activities">Ver más</a>
                        </div> 
                    </div> 
                    <div class="col-md-3 col-sm-6 text-center"> 
                        <div class="category"> 
                            <i class="icon icon-streetsign"></i> 
                            <h5 class="text-uppercase category-title">
                                Traslados y embarques en cada vuelo de ida/vuelta
                            </h5> 
                            <p style="margin: 0px;"> 
                                Traslados en destino siempre auxiliados por personal de kanvoy, y en origen en caso de ser solicitado el servicio. 
                            </p>                            
                            <a class="btn text-uppercase" href="<?= base_url() ?>p/actividades#trip-actividades-incluidas">Ver más</a>
                        </div>
                    </div> 
                    <div class="col-md-3 col-sm-6 text-center"> 
                        <div class="category"> 
                            <i class="icon icon-bike"></i> 
                            <h5 class="text-uppercase category-title">Alojamientos y actividades</h5> 
                            <p style="margin: 0px;"> 
                                Alojamientos y actividades, garantizados y reservados con los mejores hoteles y actividades diurnas de la isla.
                            </p>
                            <a class="btn text-uppercase" href="<?= base_url() ?>p/actividades#trip-actividades-extras">Ver más</a>
                        </div> 
                    </div>
                    <div class="col-md-3 col-sm-6 text-center"> 
                        <div class="category"> 
                            <i class="icon icon-megaphone"></i> 
                            <h5 class="text-uppercase category-title">Asistencia en origen y destino</h5> 
                            <p style="margin: 0px;"> 
                                Asistencia en origen y en destino, con auxilio del viajero tanto a la ida como a la vuelta y en destino, y en vuelo. 
                            </p>
                            <a class="btn text-uppercase" href="<?= base_url() ?>p/actividades#trip-seguros">Ver más</a>
                        </div> 
                        </div>
                    <div class="col-md-3 col-sm-6 text-center"> 
                        <div class="category"> 
                            <i class="icon icon-wine"></i> 
                            <h5 class="text-uppercase category-title">
                                Organización de fiestas y excursiones
                            </h5> 
                            <p style="margin: 0px;"> 
                                Organización de fiestas y excursiones según posibilidades y demanda de los viajeros. 
                            </p>
                            <a class="btn text-uppercase" href="<?= base_url() ?>p/actividades#trip-activities">Ver más</a>
                        </div> 
                    </div>
                    <div class="col-md-3 col-sm-6 text-center"> 
                        <div class="category"> 
                            <i class="icon icon-trophy"></i> 
                            <h5 class="text-uppercase category-title">
                                Gestión y producción de merchandising y sorteos
                            </h5> 
                            <p style="margin: 0px;"> 
                                Gestion y producción de merchandising. Buscamos y confeccionamos los productos que os interesen, y los adaptamos a vuestras peticiones.
 
                            </p>
                            <a class="btn text-uppercase" href="<?= base_url() ?>p/actividades#trip-material-viaja-gratis">Ver más</a>
                        </div> 
                        </div>
                    <div class="col-md-3 col-sm-6 text-center"> 
                        <div class="category"> 
                            <i class="icon icon-flag"></i> 
                            <h5 class="text-uppercase category-title">
                                Y un largo etc, que hará de vuestro viaje inolvidable
                            </h5> 
                            <p style="margin: 0px;"> 
                                Y un largo etc, que hará que vuestro viaje sea inolvidable. 
                            </p>
                            <a class="btn text-uppercase" href="<?= base_url() ?>p/contactenos">Ver más</a>
                        </div> 
                    </div>
                 
                </div> 
            </div> 
        </div> <!-- /.trip categories --> 
    </section> 
    <section class="about"> 
        <div class="about-video-bg"> 
            <div id="player"></div> 
        </div> 
        <div class="container"> 
            <div class="row text-center"> 
                <div class="col-md-offset-8 col-md-4 col-sm-offset-6 col-sm-6"> 
                    <p class="about-foundation" style="margin: 0px;"> 
                        <span>Desde </span>
                        <span class="divc">anticlericalism</span>2010 
                    </p>
                    <p class="lefty" style="margin: 0px;">crystallographical</p>
                    <span class="fitz">dynamotor unsegregated</span>
                    <span class="frow">semibouffant</span> 
                </div> 
            </div> 
            <div class="row text-center"> 
                <div data-wow-duration="1s" data-wow-delay="0.2s" class="col-md-offset-8 col-md-4 col-sm-offset-6 col-sm-6 wow animated fadeIn" style="animation-duration: 1s; animation-delay: 0.2s; animation-name: none;"> 
                    <div class="about-description"> 
                        <i class="icon icon-telescope"></i> 
                        <?= $this->db->get_where('paginas',array('id'=>10))->row()->contenido;  ?>
                        <button id="about-play" data-wow-delay="0.1s" class="btn play-btn text-uppercase wow animated slideInRight hovered" style="animation-delay: 0.1s; animation-name: none;"> 
                            Mira nuestro video 
                        </button> 
                        <button id="about-stop" class="btn play-btn text-uppercase hovered"> Parar video </button> 
                    </div> 
                </div> 
            </div> 
        </div> 
    </section> 
    <section class="our-team"> 
        <div class="container"> <!-- section-intro --> 
            <div class="row text-center section-intro"> 
                <div class="col-md-offset-2 col-md-8"> 
                    <i data-wow-delay="0.2s" class="icon icon-puzzle wow animated fadeInUp" style="animation-delay: 0.2s; animation-name: none;"></i>
                    <h1 data-wow-delay="0.4s" class="text-uppercase wow animated fadeInUp" style="animation-delay: 0.4s; animation-name: none;">NUESTRO EQUIPO</h1>
                    <p style="margin: 0px;">Estas personas hacen que el viaje sea increible</p>
                    <p class="vice" style="margin: 0px;">pinguidity</p>
                    <span class="coma">agitable detribalise</span>
                    <span class="poll">hematocyst</span> 
                </div> 
            </div> <!-- /.section-intro --> <!-- team members --> 
            <div class="row"> 
                

                <?php 
                    $this->db->order_by('priority','ASC'); 
                    foreach($this->db->get_where('galeria',array('categorias_galeria_id'=>2))->result() as $g): 
                ?>
                <div class="col-md-3 col-sm-6 text-center"> 
                    <div class="team-member"> 
                        
                        <div style="border:10px solid #f71259; background:white; padding:10px;">
                            <div style="background:url(<?= base_url() ?>images/galeria/<?= $g->foto ?>); background-size:cover; background-position: center; height:250px;">
                                <img alt="team member photo" class="img-responsive member-photo" src="<?= base_url() ?>images/galeria/<?= $g->foto ?>" style="visibility: hidden;"> 
                            </div>
                        </div>


                        <h5 class="text-uppercase"><?= @explode('|',$g->titulo)[0] ?></h5> 
                        <span class="text-capitalize"><?= @explode('|',$g->titulo)[1] ?></span>
                        <span class="bura">unmilitarised</span> 
                        <ul class="list-inline member-socials">
                            <li> <a href="#"><i aria-hidden="true" class="fa fa-facebook-square"></i></a>
                                <p class="coma" style="margin: 0px;">underscoring</p>
                                <span class="lehi">overspicing vectorially</span> 
                            </li> 
                            <li> 
                                <a href="#"><i aria-hidden="true" class="fa fa-twitter-square"></i></a>
                                <p class="kiwi" style="margin: 0px;">nonponderous</p>
                                <span class="sown">undermelody nonannexable</span> 
                            </li> 
                            <li>
                                <a href="#">
                                    <i aria-hidden="true" class="fa fa-instagram"></i>
                                </a>
                                <p class="coma" style="margin: 0px;">anticlericalism</p>
                                <span class="frow">unhuntable nonconjunction</span> 
                            </li> 
                        </ul> 
                    </div> 
                </div> 
                <?php endforeach ?>


                
            

            </div> <!-- /.team members --> <!-- section-intro --> 
            <div class="row text-center section-intro"> 
                <div class="col-md-offset-2 col-md-8"> 
                    <i data-wow-delay="0.2s" class="icon icon-speedometer wow animated fadeInUp" style="animation-delay: 0.2s; animation-name: none;"></i> 
                    <h1 data-wow-delay="0.4s" class="text-uppercase wow animated fadeInUp" style="animation-delay: 0.4s; animation-name: none;">
                        Satisfacción garantizada
                    </h1> 
                    <p style="margin: 0px;">Hará de vuestro viaje una experiencia inolvidable</p>
                    <p class="lefty" style="margin: 0px;">conclusiveness</p><span class="lehi">familiarised controversial</span>
                    <span class="lefty">pseudosubtle</span> 
                </div> 
            </div> <!-- /.section-intro --> <!-- tips -->
            <div class="row"> 
                <div class="col-md-4 text-center">
                    <div class="tip"> <i class="icon icon-wallet"></i> 
                        <h5 class="text-uppercase">VIAJE SEGURO</h5> 
                        <p style="margin: 0px;">Contamos con una gran cobertura sanitaria y médica, de equipaje, de transporte, etc, con posibilidad de ampliarla.</p>
                        <p class="divc" style="margin: 0px;">gloriousness</p>
                        <span class="classy">euphemia ununitable</span>
                        <span class="bura">noninfantry</span> 
                    </div> 
                </div> 
                <div class="col-md-4 text-center"> 
                    <div class="tip"> 
                        <i class="icon icon-laptop"></i> 
                        <h5 class="text-uppercase">Soporte informativo</h5> 
                        <p style="margin: 0px;">Facilitamos los trámites y pagos on-line a través de un usuario en éste mismo sitio web.</p>
                        <p class="coma" style="margin: 0px;">nonannexable</p>
                        <span class="frow">philosophicalness angledozer</span>
                        <span class="libeled">unobsolete</span> 
                    </div> 
                </div> 
                <div class="col-md-4 text-center"> 
                    <div class="tip"> 
                        <i class="icon icon-happy"></i> 
                        <h5 class="text-uppercase">Buen ambiente</h5> 
                        <p style="margin: 0px;">Garantizamos una experiencia inolvidable con diversión, amigos/as y fiesta incluida!</p>
                        <p class="frow" style="margin: 0px;">annexationism</p>
                        <span class="sown">bathymetry underscoring</span>
                        <span class="classy">cyclopentadiene</span> 
                    </div> 
                </div> 
            </div> <!-- /.tips --> 
        </div>
    </section>     
    <?= $this->load->view('includes/template/contact') ?>
    <button class="btn goUp-btn"> 
        <i class="fa fa-angle-up"></i> 
        <span>SUBIR</span>
        <span class="fitz">noninfantry</span> 
    </button> <!-- /.go up arrow -->
</main>
<?= $this->load->view('includes/template/footer'); ?>
</div>