<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
        
        function pagos_clientes(){            
            $crud = $this->crud_function('',''); 
            if($crud->getParameters()=='list'){
                $crud->field_type('status','dropdown',array(
                    -1=>'<span class="label label-danger">No Procesado</span>',
                    1=>'<span class="label label-info">Por procesar</span>',
                    2=>'<span class="label label-success">Procesado</span>'
                ));
            }else{
                $crud->field_type('status','dropdown',array(
                    -1=>'No Procesado',
                    1=>'Por procesar',
                    2=>'Procesado'
                ));
            }
            
            $crud->display_as('monto','Importe');
            $crud = $crud->render();
            $crud->title = 'Registro de pagos';                        
            $this->loadView($crud);
        }
        
        function tokenizar_pago(){
            $this->form_validation->set_rules('id','ID','required|integer');
            if($this->form_validation->run()){     
                $pedido = $this->db->get_where('pagos_clientes',array('id'=>$this->input->post('id')));
                if($pedido->num_rows()>0){
                    
                    $idrand = date("s").$pedido->row()->id;
                    //$idrand = 491;
                    
                    $importe = $pedido->row()->monto*100;
                    $urlok = base_url('pagos/frontend/pagook');
                    $urlnook = base_url('pagos/frontend/pagonok');
                    $entidad  = "0000554026";
                    $comercio = "081534372";
                    $terminal = "00000003";
                    //$clave = "5AU6I1DU"; //Test
                    $clave = "TA9TS91E"; //Produccion
                    //https://comercios.ceca.es/ 081534372 kanvoy2019
                    //Tarjeta 5540500001000004 AAAA12 989
                    $moneda = "978";
                    $exponente = "2";
                    $last = $this->db->get_where('pagos_clientes',array('id'=>$_POST['id']));
                    if($last->num_rows()>0){
                        $last = (array)$last->row();
                        unset($last['id']);
                        //$this->db->insert('pagos_clientes',$last);
                    }
                    $this->db->update('pagos_clientes',array('nro_pago'=>$idrand),array('id'=>$_POST['id']));                    
                    $code = $clave.$comercio.$entidad.$terminal.$idrand.$importe.$moneda.$exponente."SHA1".$urlok.$urlnook;
                    $signature = sha1($code);
                    echo json_encode(array('code'=>$code,'importe'=>$importe,'id'=>$idrand,'firma'=>$signature));
                }
            }
        }
    }
?>
