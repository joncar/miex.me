<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Frontend extends Main{
        function __construct() {
            parent::__construct();
        }                        
        
        function pagook(){
            $this->loadView(array('view'=>'panelUsuario','crud'=>'user','title'=>'Pago satisfactorio','output'=>$this->load->view('pagook',array(),TRUE)));
        }
        function pagonok(){
            $this->loadView(array('view'=>'panelUsuario','crud'=>'user','title'=>'Pago no aprobado','output'=>$this->load->view('pagonok',array(),TRUE)));
        }
    }
?>
