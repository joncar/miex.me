<div class="loaded" id="page-content"> 
    <header class="overlay"> <!-- navigation / main menu --> 
        <?= $this->load->view('includes/template/menu2') ?>
    </header> <!-- main content --> 
    <main> 
        <section style="padding-top: 117px; height: 100%;"> 
            <div class="container">
                <h1>Zona Usuario</h1>                
                <div class="row" style="margin:20px;">
                    <div id="main" class="alert alert-success">    
                        <!-- /section -->            
                        Pago procesado con éxito
                    </div>
                </div>
            </div> 
        </section> <!-- go up arrow --> 
        <button class="btn goUp-btn"> 
            <i class="fa fa-angle-up"></i> <span>Go Up</span><span class="mydiv">variolitic</span> 
        </button> <!-- /.go up arrow --> 
        <?php $this->load->view('includes/scripts',array('removeFunction'=>true)); ?>
    </main>    
</div>