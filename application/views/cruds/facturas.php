<?php if($action=='add' || $action == 'edit'): ?>
    <form class="flexigrid" accept-charset="utf-8" enctype="multipart/form-data" autocomplete="off" id="crudForm" method="post" action="<?= base_url() ?>perceptoria/facturacion/facturas/insert">
    <?= $output[0]->output ?>
    <div style="margin-top:10px; border-top:1px solid lightgray; padding:20px;">
        <div class="panel-group accordiondetalles" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default tabDetalle">
                  <div class="panel-heading" role="tab" id="heading0">
                    <h4 class="panel-title row">
                        <div class="col-xs-6 col-sm-11">
                            <?= form_dropdown_from_query('arancel_id_0_facturas_detalles','arancel','id','arancel_nombre',0,'data-table="facturas_detalles" data-name="arancel_id"',TRUE,'arancel_id') ?>
                        </div>
                        <div class="col-xs-6 col-sm-1" align="right">
                            <a role="button" class="plusbtn" data-toggle="collapse" data-parent="#accordion" href="#collapse0" aria-expanded="true" aria-controls="collapse0">
                                <i class="fa fa-arrow-circle-down "></i>
                            </a>
                            <a role="button" class="plusrmbtn" style="color:red; display:none;" href="#">
                                <i class="fa fa-minus-circle"></i>
                            </a>
                        </div>
                    </h4>
                  </div>
                  <div id="collapse0" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading0">
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-6">                        
                                <div class='form-group' id="detalles_field_box">
                                        <label for="field-derecho_arancel_detalle_id">
                                            Derecho Arancelario
                                        </label>
                                        <?php 
                                            $query = get_instance()->db->get('derecho_arancel_detalle');                                            
                                            $label = 'denominacion';
                                            $first = 0;
                                            echo '<select name="derecho_arancel_detalle_id_0_facturas_detalles" data-table="facturas_detalles" data-name="derecho_arancel_detalle_id" class="form-control chosen-select derecho_arancel_detalle">';
                                            echo '<option value="">Seleccione una opción</option>';
                                            foreach($query->result() as $q){
                                                $sel = $first==$q->id?'selected':'';
                                                $file = empty($q->formulario)?'':'data-file="'.$q->formulario.'"';
                                                echo '<option value='.$q->id.' data-val='.$q->monto.' '.$sel.' '.$file.'>'.$q->denominacion.'</option>';
                                            }
                                            echo '</select>';
                                        ?>                                        
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-2">
                                <div class='form-group' id="detalles_field_box">
                                        <label for="field-derecho_arancel_detalle_id">
                                            Cantidad
                                        </label>
                                        <input type="number" name="cant_0_facturas_detalles" id="field-cantidad" class="form-control cantidad" value="1" data-name="cant" data-table="facturas_detalles">
                                </div>                        
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-2">
                                <div class='form-group' id="detalles_field_box">
                                        <label for="field-derecho_arancel_detalle_id">
                                            Monto
                                        </label>
                                        <input type="number" name="monto_0_facturas_detalles" id="field-monto" class="form-control monto" value="0" data-name="monto" data-table="facturas_detalles" readonly>
                                </div>                        
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-2">
                                <div class='form-group' id="detalles_field_box">
                                        <label for="field-derecho_arancel_detalle_id">
                                            Total
                                        </label>
                                        <input type="number" name="total_0_facturas_detalles" id="field-total" class="form-control total" value="0" data-name="total" data-table="facturas_detalles">
                                </div>                        
                            </div>
                        </div>
                        <div class="ajaxResponse"></div>                            
                    </div>
                  </div>
                </div>
        </div>
    </div>
    <input type="hidden" name="filas_facturas_detalles" id="filas" value="0">
    <input id="field-facturas_id" type="hidden" name="facturas_id" value="facturas.id" class="form-control" style="width: 100%;">
    <div style="margin-left: 0px; margin-right: 0px; margin-top: 30px;" id="buttons" class="row">
            <div class="alert alert-danger" style="display:none" id="report-error"></div>
            <div class="alert alert-success" style="display:none" id="report-success"></div>
            <div aria-label="..." role="group" class="btn-group btn-group-justified">
                <div role="group" class="btn-group">
                  <button class="btn btn-success" type="submit">Guardar</button>
                </div>
                <div role="group" class="btn-group">
                  <button class="btn btn-default" id="save-and-go-back-button" type="button">Guardar y volver a la lista</button>
                </div>
                <div role="group" class="btn-group">
                  <button class="btn btn-danger" id="cancel-button" type="button">Cancelar</button>
                </div>
           </div>
    </div>
</form>
<script>
    $(document).on('ready',function(){
        $("#buttons").remove();
        $("#field-iva5, #field-iva10, #field-exenta, #field-total_factura").val(0);
        $(".chzn-container, .chzn-drop, .chzn-search input").css('width','100%');
        $(document).on('change','#field-categoria_arancel_id',function(){
            switch($(this).val()){
                case '1': //Estudiante
                    $("#programacion_carreras_id_field_box").show();
                break;
                default:
                    $("#programacion_carreras_id_field_box").hide();
                    $("#field-programacion_carreras_id").val('');
                break;
            }
        });
        $(document).on('change','.arancel_id',function(){
            if($(this).val()!=='' && $("#field-anho_lectivo").val()!==''){
                var o = $(this);
                $.post('<?= base_url('perceptoria/facturacion/getAranceles') ?>',{facultades_id:$("#field-facultades_id").val(),categoria_arancel_id:$("#field-categoria_arancel_id").val(),arancel_id:$(this).val(),anho_lectivo:$("#field-anho_lectivo").val(),programacion_carreras_id:$("#field-programacion_carreras_id").val()},function(data){
                    //Añadir relacion                    
                    var ht = o.parents('.tabDetalle');
                    ht.find('.collapse').addClass('in');
                    ht.find('.derecho_arancel_detalle').parent().html(data);
                    ht.find('.derecho_arancel_detalle').change(function(){
                        $(this).trigger('calcularDerecho');
                    });
                    var newht = ht.clone();
                    newht.find('.arancel_id').val('');                    
                    newht.find('select').removeAttr('id');
                    newht.find('select').removeClass('chzn-done');                
                    newht.find('.chzn-container').remove();                
                    ht.after(newht);                
                    newht.find('select').chosen({"search_contains": true, allow_single_deselect:true});	
                    var id = newht.find('.arancel_id').attr('id');
                    newht.find(".panel-heading").attr('id','heading'+id);
                    newht.find(".plusbtn").attr('href','#collapse'+id);
                    newht.find(".collapse").attr('id',"collapse"+id);
                    newht.find(".collapse").removeClass('in');
                    $(".chzn-container").css('width','100%');
                    newht.find('.plusrmbtn').show();
                    refresh();
                });                
            }
        });
        
        $(document).on('change, calcularDerecho','.derecho_arancel_detalle, .cantidad',function(){
           var el = $(this).parents('.panel-body');
           el.find('.monto').val(el.find('.derecho_arancel_detalle option:selected').data('val'));
           el.find('.total').val(parseFloat(el.find('.monto').val())*parseFloat(el.find('.cantidad').val()));
           $(document).trigger('total');
           var form = el.find('.derecho_arancel_detalle option:selected').data('file');
           var data = {user:$("#field-user_id").val(), anho_lectivo:$("#field-anho_lectivo").val(),facultades_id:$("#field-facultades_id").val()};
           data = JSON.stringify(data);
           if(form!==undefined){
               if($("#field-user_id").val()===''){
                   alert('Por favor seleccione un usuario para poder obtener los datos del formulario');
               }else{
                    $.post('<?= base_url('perceptoria/facturacion/getForm') ?>',{name:form,data:data},function(data){
                        el.find('.ajaxResponse').html(data);
                        refresh();
                    });
                }
           }else{
                el.find('.ajaxResponse').html('');
                refresh();
           }
        });
        
        $(document).on('change','#field-user',function(){
            if($(this).val()!==''){
                $.post('<?= base_url('perceptoria/facturacion/getCedula') ?>',{cedula:$(this).val()},function(data){
                    data = JSON.parse(data);
                    if(data.error!==undefined){
                        $("#field-cedula").html('<span style="color:red">'+data.error+'</span>');
                        $("#field-cedula").show();
                    }else{
                        $("#field-cedula").html(data.data);
                        $("#field-user_id").val(data.id);
                        $("#field-cedula").show();
                        $("#field-programacion_carreras_id").replaceWith(data.carrera);
                        $("#field_programacion_carreras_id_chzn").remove();
                        $("#field-programacion_carreras_id").chosen({"search_contains": true, allow_single_deselect:true});
                        $(".chzn-container").css('width','100%');
                    }
                });
            }
        });
        
        $(document).on('click','.plusrmbtn',function(e){
            e.preventDefault();
            $(this).parents('.tabDetalle').remove();
            refresh();
        });
        
        $(document).on('total',function(){
            var total = 0;
            $(".total").each(function(){
               total+= parseFloat($(this).val());
               $("#field-total_factura").val(total);
            });
        });
    });
    
    function refresh(){
        var filas = 0;
        $(".accordiondetalles .panel").each(function(){
            $(this).find('input, select').each(function(){
                if(!$(this).parent().hasClass('chzn-search')){
                    $(this).attr('name',$(this).data('name')+'_'+filas+'_'+$(this).data('table'));
                    $('#filas').val(filas);
                }
            });
            filas++;
        });
    }
</script>
<?php else: ?>
<?= $output[0]->output ?>
<?php endif ?>