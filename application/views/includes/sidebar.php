<?php if($this->user->log): ?>
<div id="sidebar" class="sidebar responsive">
        <ul class="nav nav-list">
            <li class="active highlight">
                <a href="<?= site_url('panel') ?>">
                        <i class="menu-icon fa fa-tachometer"></i>
                        <span class="menu-text">Escritorio</span>
                </a>
                <b class="arrow"></b>
            </li>
             <!--- Alumnos --->
             <?php 
                    $menu = array(
                        'paginas'=>array('admin/categorias_paginas','admin/paginas','admin/subscriptores','admin/testimonios'),
                        'galeria'=>array('admin/categorias_galeria'),
                        'discotecas'=>array('admin/discotecas'),
                        'b'=>array('blog_categorias','blog'),
                        'h'=>array('categorias','hoteles'),
                        'pagos'=>array('admin/pagos_clientes'),
                        'documentos'=>array('admin/documentos'),
                        'seguridad'=>array('ajustes','grupos','funciones','user')
                    );
                    $menu = $this->user->filtrarMenu($menu);
                    $label = array(
                        'b'=>array('Blog','fa fa-book'),
                        'h'=>array('Hoteles','fa fa-book'),
                        'galeria'=>array('Galeria','fa fa-image'),
                        'documentos'=>array('Documentos','fa fa-file'),
                        'paginas'=>array('CMS','fa fa-book'),
                        'pagos_clientes'=>array('Pagos','fa fa-money'),
                        'seguridad'=>array('Seguridad','fa fa-user-secret')
                    );
             ?>
             <?php  echo getMenu($menu,$label); ?>            
        </ul>
       <div id="sidebar-collapse" class="sidebar-toggle sidebar-collapse">
            <i data-icon2="ace-icon fa fa-angle-double-right" data-icon1="ace-icon fa fa-angle-double-left" class="ace-icon fa fa-angle-double-left"></i>
        </div>

        <script type="text/javascript">
                try{ace.settings.check('sidebar' , 'collapsed')
                ace.settings.sidebar_collapsed(true, true);
                }catch(e){}
        </script>
</div>
<?php endif ?>
