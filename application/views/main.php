<div class="first-page loaded" id="page-content"> <!-- first page header with video bg --> 
<header class="overlay"> <!-- navigation / main menu --> 
    <?= $this->load->view('includes/template/menu') ?>
    <div class="video_bg"> 
        <video preload="metadata" id="my-video" poster="<?= base_url() ?>images/bg1.jpg" muted="" loop="" autoplay=""> <!-- video source --> 
            <source type="video/mp4" src="<?= base_url() ?>video/<?= $this->db->get('ajustes')->row()->video_main_mp4 ?>"></source> 
            <source type="video/ogv" src="<?= base_url() ?>video/<?= $this->db->get('ajustes')->row()->video_main_ogv ?>"></source> 
        </video> 
    </div> 
    <div class="header-center-content"> 
        <div class="container text-center"> 
            <div class="row"> 
                <div class="col-md-offset-2 col-md-8 animated fadeInUp"> 
                    <span class="logo text-uppercase">
                        <img src="<?= base_url() ?>images/logo-web.png" style="width:100%; height:auto"></i>
                    </span>
                    <span class="lefty">posthemiplegic</span> 
                    <h1 class="text-uppercase">MALLORCA ISLAND EXPERIENCE</h1> 
                    <h4>Selecciona el mejor destino y vive experiéncia increible. Nosotros te lo preparamos!</h4> 
                </div> 
            </div> 
            <div class="row">
                <form action="<?= base_url('hoteles') ?>" method="get">
                    <div class="col-md-offset-3 col-md-6 col-sm-offset-2 col-sm-8 select-options">   
                        <button class="btn search-btn animated fadeInUp"> <i class="fa fa-search"></i> </button>
                        <select name="categorias_hoteles_id" class="cs-select cs-skin-elastic experience-select animated fadeInUp" id="experience"> 
                            <option selected="" disabled="" value="0">Mallorca</option> 
                            <?php foreach($this->db->get_where('categorias_hoteles')->result() as $c): ?>
                                <option value="<?= $c->id ?>"><?= $c->categorias_hoteles_nombre ?></option>
                            <?php endforeach ?>
                        </select>
                        <select name="descripcion" class="cs-select cs-skin-elastic destination-select animated fadeInUp" id="destionation"> 
                            <option selected="" disabled="" value="0">Hotel</option> 
                            <?php foreach($this->db->get_where('hoteles')->result() as $c): ?>
                                <option value="<?= $c->id ?>"><?= $c->nombre ?></option>
                            <?php endforeach ?>
                        </select>
                    </div> 
                </form>
            </div> 
        </div> 
    </div> 
    <button class="btn scroll-down animated fadeInDown"><i aria-hidden="true" class="fa fa-arrow-circle-down"></i></button> 
</header>
<main> 
    <section class="top-destination"> 
        <div class="container"> <!-- section-intro --> 
            <div class="row text-center section-intro"> 
                <div class="col-md-offset-2 col-md-8">
                    <i data-wow-delay="0.2s" class="icon icon-anchor wow  fadeInUp animated" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;"></i> 
                    <h1 data-wow-delay="0.4s" class="text-uppercase wow  fadeInUp animated" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
                        DESTINOS EN MALLORCA
                    </h1> 
                    <p data-wow-delay="0.4s" class="wow  fadeInUp animated" style="margin: 0px; visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
                        Explora los hoteles y sus zonas
                    </p>
                    <p class="mydiv" style="margin: 0px;">prepenetration</p>
                    <span class="coma">stupefacient gloriousness</span>
                    <span class="divc">amylogen</span> 
                </div> 
            </div> <!-- /.section-intro --> <!-- country list --> 
            <div class="row"> 
                <ul class="col-md-12 text-center text-uppercase list-inline country-list">                     
                    <?php foreach($this->db->get_where('categorias_hoteles')->result() as $c): ?>
                        <li> <span><?= $c->categorias_hoteles_nombre ?></span> <i><?= $this->db->get_where('hoteles',array('categorias_hoteles_id'=>$c->id))->num_rows() ?></i> </li> 
                    <?php endforeach ?>
                </ul> 
            </div> <!-- /.country list --> 
        </div> <!-- map --> 
        <div id="map" data-tap-disabled= true></div>
        <!-- /.map --> 
    </section> 
    <section data-bg=<?= base_url() ?>images/advenure-bg.jpg" class="adventure-select overlay" style="background-image: url(<?= base_url() ?>images/advenure-bg.jpg);"> 
        <div class="container"> <!-- section-intro --> 
            <div class="row text-center section-intro"> 
                <div class="col-md-offset-2 col-md-8"> 
                
                    <i data-wow-delay="0.2s" class="icon icon-compass wow  fadeInUp animated" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;"></i> 
                    <h1 data-wow-delay="0.4s" class="text-uppercase wow  fadeInUp animated" style="visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
                        EXPLORA MIEX CON NOSOTROS
                    </h1> <p data-wow-delay="0.4s" class="wow  fadeInUp animated" style="margin: 0px; visibility: visible; animation-delay: 0.4s; animation-name: fadeInUp;">
                        Descubre el mejor viaje a Mallorca de tu vida
                    </p>
                    <p class="sown" style="margin: 0px;">natatoriums</p>
                    <span class="kiwi">aetiology unprefaced</span>
                    <span class="kiwi">nonponderous</span> 
                </div> 
            </div> <!-- /.section-intro --> 
            <div class="row"> 
                <div class="col-md-6"> 
                    <div data-wow-duration="1s" class="author-quote wow  fadeIn animated" style="visibility: visible; animation-duration: 1s; animation-name: fadeIn;"> <h3 data-wow-delay="0.2s" class="text-uppercase wow  slideInLeft" style="visibility: visible; animation-delay: 0.2s; animation-name: slideInLeft;">
                            POR QUÉ VIAJAR CON NOSOTROS? 
                        </h3> 
                        <hr data-wow-delay="0.2s" class="wow  slideInLeft" style="visibility: visible; animation-delay: 0.2s; animation-name: slideInLeft;"> 
                        <i data-wow-delay="0.3s" class="icon icon-megaphone wow  slideInLeft" style="visibility: visible; animation-delay: 0.3s; animation-name: slideInLeft;"></i> 
                        <h3 data-wow-delay="0.3s" class="wow  slideInLeft" style="visibility: visible; animation-delay: 0.3s; animation-name: slideInLeft;"> 
                            <i>Damos toda la confianza de Kanvoy y sus proveedores, personal propio, mejores hoteles, actividades, asistencia 24 horas...
</i> 
                        </h3> 
                        <small data-wow-delay="0.4s" class="text-uppercase wow  slideInLeft" style="visibility: visible; animation-delay: 0.4s; animation-name: slideInLeft;">
                        </small> 
                    </div> 
                </div> 
                <div class="col-md-6"> 
                    <ul class="list-inline text-uppercase adventure-list"> 
                        <li  class="wow  fadeInUp animated"> 
                            <a href="<?= base_url('p/sobre-nosotros') ?>"> 
                                <img class="img-responsive" alt="adventure-image" src="images/zadv1.jpg"> 
                                <div class="overlay-lnk text-uppercase text-center"> 
                                    <i class="icon icon-gift"></i> 
                                    <h5>¿Que es MIEX?</h5> 
                                </div> 
                            </a>
                        </li> 
                        <li  class="wow  fadeInUp animated"> 
                            <a href="<?= base_url('p/actividades#trip-activities') ?>"> 
                                <img class="img-responsive" alt="adventure-image" src="images/zadv2.jpg"> 
                                <div class="overlay-lnk text-uppercase text-center"> 
                                    <i class="icon icon-key"></i> 
                                    <h5>¿Que incluye?</h5> 
                                </div> 
                            </a>
                        </li>
                        <li  class="wow  fadeInUp animated"> 
                            <a href="<?= base_url('p/actividades#trip-activities') ?>"> 
                                <img class="img-responsive" alt="adventure-image" src="images/zadv3.jpg"> 
                                <div class="overlay-lnk text-uppercase text-center"> 
                                    <i class="icon icon-lightbulb"></i> 
                                    <h5>¿PORQUE MIEX?</h5>  
                                </div> 
                            </a>
                        </li>
                         <li  class="wow  fadeInUp animated"> 
                            <a href="<?= base_url('p/actividades#trip-preguntas-frecuentes') ?>"> 
                                <img class="img-responsive" alt="adventure-image" src="images/zadv4.jpg"> 
                                <div class="overlay-lnk text-uppercase text-center"> 
                                    <i class="icon icon-chat"></i> 
                                    <h5>PREGUNTAS FRECUENTES</h5> 
                                </div> 
                            </a>
                        </li>
                    </ul> 
                </div> 
            </div> 
        </div> 
    </section> 
    <section class="about"> 
        <div class="about-video-bg">
            <div id="player"></div> 
        </div> 
        <div class="container"> 
            <div class="row text-center"> 
                <div class="col-md-offset-8 col-md-4 col-sm-offset-6 col-sm-6"> 
                    <p class="about-foundation" style="margin: 0px;"> 
                        <span>Desde </span>
                        <span class="coma">nonexplicable</span>2010  
                    </p>
                    <p class="lehi" style="margin: 0px;">zaporozhye</p>
                    <span class="lefty">overdiscouraging nonconjunction</span>
                    <span class="sown">superadornment</span> 
                </div>
            </div> 
            <div class="row text-center"> 
                <div data-wow-duration="1s" data-wow-delay="0.2s" class="col-md-offset-8 col-md-4 col-sm-offset-6 col-sm-6 wow  fadeIn animated" style="visibility: visible; animation-duration: 1s; animation-delay: 0.2s; animation-name: fadeIn;"> <div class="about-description"> <i class="icon icon-telescope"></i> 
                        <h1 class="text-uppercase">QUIENES SOMOS</h1> 
                    
                        <p style="margin: 0px;"> Somos un grupo de profesionales que harán de vuestro viaje la mayor experiencia de vuestra vida.
 </p></br>
                        <span> Experiencia organizada por KANVOY para poder ofrecer una alternativa diferente  a los estudiantes de Bachillerato para su fin de curso.
                                </span>
                        <button id="about-play" data-wow-delay="0.1s" class="btn play-btn text-uppercase wow  slideInRight hovered" style="visibility: visible; animation-delay: 0.1s; animation-name: slideInRight;"> Mira nuestro video </button> 
                        <button id="about-stop" class="btn play-btn text-uppercase hovered"> Parar Video </button> 
                    </div> 
                </div> 
            </div> 
        </div> 
    </section>
    <section class="blog-posts"> 
        <div class="container"> 
            <div class="row" style="margin-bottom:30px"> <!-- blog inro -->                 
                <div class="col-md-12" style="text-align:center">
                    <h1>
                        <i class="icon icon-dial" style="font-size:60px;  padding-bottom: 30px; color:#f71259;"></i> <br/>
                        <h1 data-wow-delay="0.2s" class="text-uppercase wow fadeInLeft" style="letter-spacing: 4px; visibility: visible; animation-delay: 0.2s; animation-name: fadeInLeft;">
                            ESTAMOS CONECTADOS
                        </h1> 
                </div>
            </div>
            <div class="row"> <!-- blog inro -->                 
                <div class="col-xs-12 col-md-4" style="border-left:10px solid #f71259;border-right:10px solid #f71259;margin-top: 20px">
                    <div style="color:gray; font-size:16px; font-weight:bold;">
                        <i class="fa fa-facebook-square" style="font-size:20px; margin-left: 30px; padding-bottom: 10px"></i> FACEBOOK
                    </div>
                    
                    <div style="text-align:center">
                        <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fkanvoy1%2F&tabs=timeline&width=340&height=620&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId=193203720852077" width="298 " height="620" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
                    </div>
                </div> <!-- /.blog posts list -->
                <div class="col-xs-12 col-md-4" style="border-right:10px solid #f71259; margin-top: 20px">
                    <div style="color:gray; font-size:16px; font-weight:bold;">
                        <i class="fa fa-twitter" style="font-size:20px; margin-left: 30px; padding-bottom: 10px"></i> TWITTER
                    </div>             
                    <div style="text-align:center">
                        <a class="twitter-timeline" data-width="298" data-height="620" href="https://twitter.com/_kanvoy">Tweets by _kanvoy</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
                    </div>
                </div> <!-- /.blog posts list -->
                <div class="col-xs-12 col-md-4" style="border-right:10px solid #f71259;margin-top: 20px"> 
                    <div style="color:gray; font-size:16px; font-weight:bold;">
                        <i class="fa fa-youtube-square" style="font-size:20px; margin-left: 30px; padding-bottom: 10px;"></i> YOUTUBE
                    </div>                    
                    <div class="social-feed-container" style="text-align:center">                            
                    </div>
                </div> <!-- /.blog posts list -->
            </div>
        </div> 
    </section>
    <section class="blog-posts" style="padding-top:80px;"> 
        <div class="container"> 
            <div class="row"> <!-- blog inro --> 
                <div class="col-md-4 "> 
                    <div class="blog-intro"> 
                        <i data-wow-delay="0.1s" class="icon icon-happy wow fadeInLeft" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInLeft;"></i> 
                        <h1 data-wow-delay="0.2s" class="text-uppercase wow fadeInLeft" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInLeft;">
                            La fiesta esta preparada y te está esperando
                        </h1> 
                        <p data-wow-delay="0.3s" class="wow fadeInLeft" style="margin: 0px; visibility: visible; animation-delay: 0.3s; animation-name: fadeInLeft;"> 
                            Recibe consejos de otros viajeros que ya han estado allí y hecho eso. Inspiración, consejos y trucos. 
                        </p>
                        <a data-wow-delay="0.4s" class="text-uppercase text-center btn read-btn wow fadeInLeft" href="<?= base_url('blog') ?>">Lee nuestro blog</a>
                    </div> 
                </div> <!-- /.blog-intro --> <!-- blog posts list --> 
                <div class="col-md-8"> 
                    <ul class="posts-list list-inline"> 
                        <?php $this->db->limit('4'); $this->db->order_by('id','DESC'); foreach($this->db->get('blog')->result() as $b): ?>
                        <li> 
                            <a href="<?= site_url('blog/'.toURL($b->id.'-'.$b->titulo)) ?>"> 
                                <img class="img-responsive" alt="post-image" src="<?= base_url('img/fotos/'.$b->foto) ?>"> 
                                <h6 class="post-intro text-center text-uppercase" ><?= $b->titulo ?></h6> 
                                <span class="text-uppercase">Leer articulo</span>
                            </a>
                        </li>
                        <?php endforeach ?>
                    </ul>
                </div> <!-- /.blog posts list -->
            </div>
        </div> 
    </section> 







<section class="small-section cws_prlx_section bg-blue-40">
    <img src="<?= base_url() ?>pic/parallax-2.jpg" alt="" class="cws_prlx_layer">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <h6 class="title-section-top font-4">Happy Memories</h6>
                    <h1 class="title-section alt-2"><span>Our</span> Testimonials</h1>
                    <div class="cws_divider mb-25 mt-5">
                        <br>
                    </div>
                </div>
            </div>
            <div class="row">
                <!-- testimonial carousel-->
                <div class="owl-three-item">
                    
                    <?php foreach($this->db->get_where('testimonios')->result() as $t): ?>
                        <!-- testimonial item-->
                        <div class="testimonial-item">
                            <div class="testimonial-top">
                                <div class="pic">
                                    <img src="<?= base_url('img/testimonios/'.$t->foto_banner) ?>" alt="">
                                </div>
                                <div class="author">
                                    <img src="<?= base_url('img/testimonios/'.$t->foto_autor) ?>" alt="">
                                </div>
                            </div>
                            <!-- testimonial content-->
                            <div class="testimonial-body">
                                <h1 class="title"><?= $t->autor ?></h1>
                                <div class="stars stars-<?= $t->estrellas ?>">
                                    <br>
                                </div>
                                <p class="align-center">
                                    <?= $t->testimonio ?>
                                </p>                            
                            </div>
                        </div>
                    <?php endforeach ?>
                </div>
            </div>
        </div>
    </section> 












    <?= $this->load->view('includes/template/contact') ?>
    <button class="btn goUp-btn"> 
        <i class="fa fa-angle-up"></i> 
        <span>SUBIR</span>
        <span class="fitz">noninfantry</span> 
    </button> <!-- /.go up arrow --> 
</main>
<?= $this->load->view('includes/template/footer'); ?>
</div>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBiTQp5QUWTeD0XQpNnv3kevaKrtokkpnA" type="text/javascript"></script>
<script type="text/javascript">
    google.maps.event.addDomListener(window, 'load', initMap); 
    function initMap() {
        var mapOptions = {
            zoom: 15, 
            scrollwheel: false,
            zoomControl: true, 
            mapTypeControl: false, 
            scaleControl: false, 
            streetViewControl: false, 
            center: new google.maps.LatLng(39.67442740076737,3.0157470703125), 
            styles: [
                { "featureType": "water", "elementType": "geometry", "stylers": [ { "color": "#ededed" }, { "lightness": 17 } ] }, 
                { "featureType": "landscape", "elementType": "geometry", "stylers": [ { "color": "#f7f7f7" }, { "lightness": 20 } ] },
                { "featureType": "road.highway", "elementType": "geometry.fill", "stylers": [ { "color": "#ffffff" }, { "lightness": 17 } ] },
                { "featureType": "road.highway", "elementType": "geometry.stroke", "stylers": [ { "color": "#ffffff" }, { "lightness": 29 },{ "weight": 0.2 } ] }, 
                { "featureType": "road.arterial", "elementType": "geometry", "stylers": [ { "color": "#ffffff" }, { "lightness": 18 } ] },
                { "featureType": "road.local", "elementType": "geometry", "stylers": [ { "color": "#ffffff" }, { "lightness": 16 } ] }, 
                { "featureType": "poi", "elementType": "geometry", "stylers": [ { "color": "#f5f5f5" }, { "lightness": 21 } ] }, 
                { "featureType": "poi.park", "elementType": "geometry", "stylers": [ { "color": "#dedede" }, { "lightness": 21 } ] }, 
                { "elementType": "labels.text.stroke", "stylers": [ { "visibility": "on" }, { "color": "#ffffff" }, { "lightness": 16 } ] },
                { "elementType": "labels.text.fill", "stylers": [ { "saturation": 36 }, { "color": "#333333" }, { "lightness": 40 } ] },
                { "elementType": "labels.icon", "stylers": [ { "visibility": "off" } ] }, 
                { "featureType": "transit", "elementType": "geometry", "stylers": [ { "color": "#f2f2f2" }, { "lightness": 19 } ] }, 
                { "featureType": "administrative", "elementType": "geometry.fill", "stylers": [ { "color": "#fefefe" }, { "lightness": 20 } ] }, 
                { "featureType": "administrative", "elementType": "geometry.stroke", "stylers": [ { "color": "#fefefe" }, { "lightness": 17 }, { "weight": 1.2 } ] } 
            ] 
        }; 
        var mapElement = document.getElementById('map'); 
        var map = new google.maps.Map(mapElement, mapOptions);
        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {            
            map.setOptions({ 'draggable': false });
        }
        var image = '<?= base_url('images/map-locator.png') ?>'; 
        var markers = [];
        
        var bounds = new google.maps.LatLngBounds();
        <?php foreach($this->db->get('hoteles')->result() as $h): ?>
            var marker<?= $h->id ?> = new google.maps.Marker({ 
                icon: image,
                animation: google.maps.Animation.DROP, 
                position: new google.maps.LatLng<?= $h->ubicacion ?>, 
                map: map, 
                title: '<?= $h->nombre ?>'
            });
            bounds.extend(new google.maps.LatLng<?= $h->ubicacion ?>);
            google.maps.event.addListener(marker<?= $h->id ?>,'click',function(){
                document.location.href="<?= base_url('hoteles/'.$h->id.'-'.$h->nombre) ?>/";
            });
        <?php endforeach ?>
        map.fitBounds(bounds);
    } 
   </script> <!-- youtube video player settings --> 

<div id="barracookies">
Usamos cookies propias y de terceros que entre otras cosas recogen datos sobre sus hábitos de navegación para mostrarle publicidad personalizada y realizar análisis de uso de nuestro sitio.
<br/>
Si continúa navegando consideramos que acepta su uso. <a href="javascript:void(0);" onclick="var expiration = new Date(); expiration.setTime(expiration.getTime() + (60000*60*24*365)); setCookie('avisocookies','1',expiration,'/');document.getElementById('barracookies').style.display='none';"><b>OK</b></a><br> <a href="javascript:legal()" target="_blank" ><b style="color: rgb(170, 170, 170)">Aviso legal</b>
</div>
<!-- Estilo barra CSS -->
<style>#barracookies {display: none;z-index: 99999;position:fixed;left:0px;right:0px;bottom:0px;width:100%;min-height:40px;padding:5px;background: #333333;color:#f72859;line-height:20px;font-family:raleway, sans;font-size:12px;text-align:center;box-sizing:border-box;} #barracookies a:nth-child(2) {padding: 4px 29px;background:#4682B4;border-radius:100px;text-decoration:none;} #barracookies a {color: #fff;text-decoration: none;}</style>
<!-- Gestión de cookies-->
<script type='text/javascript'>function setCookie(name,value,expires,path,domain,secure){document.cookie=name+"="+escape(value)+((expires==null)?"":"; expires="+expires.toGMTString())+((path==null)?"":"; path="+path)+((domain==null)?"":"; domain="+domain)+((secure==null)?"":"; secure")}function getCookie(name){var cname=name+"=";var dc=document.cookie;if(dc.length>0){begin=dc.indexOf(cname);if(begin!=-1){begin+=cname.length;end=dc.indexOf(";",begin);if(end==-1)end=dc.length;return unescape(dc.substring(begin,end))}}return null}function delCookie(name,path,domain){if(getCookie(name)){document.cookie=name+"="+((path==null)?"":"; path="+path)+((domain==null)?"":"; domain="+domain)+"; expires=Thu, 01-Jan-70 00:00:01 GMT"}}</script>
<!-- Gestión barra aviso cookies -->
<script type='text/javascript'>
var comprobar = getCookie("avisocookies");
if (comprobar != null) {}
else {
var expiration = new Date();
expiration.setTime(expiration.getTime() + (60000*60*24*365));
setCookie("avisocookies","1",expiration);
document.getElementById("barracookies").style.display="block"; }
</script>






<!---- Social Feed ---->
<!-- jQuery -->
<!-- Codebird.js - required for TWITTER -->
<script src="<?= base_url() ?>js/social/bower_components/codebird-js/codebird.js"></script>
<!-- doT.js for rendering templates -->
<script src="<?= base_url() ?>js/social/bower_components/doT/doT.min.js"></script>
<!-- Moment.js for showing "time ago" and/or "date"-->
<script src="<?= base_url() ?>js/social/bower_components/moment/min/moment.min.js"></script>
<!-- Moment Locale to format the date to your language (eg. italian lang)-->
<script src="<?= base_url() ?>js/social/bower_components/moment/locale/it.js"></script>
<!-- Social-feed js -->
<script src="<?= base_url() ?>js/social/js/jquery.socialfeed.js"></script>
<script>
    $(document).ready(function(){
        $('.social-feed-container').socialfeed({            
            // INSTAGRAM
            youtube:{
                accounts: ['UCMpBfRuhSaCajp2--XAASKw'],  //Array: Specify a list of accounts from which to pull wall posts
                limit: 2,                                   //Integer: max number of posts to load
                avatar:'https://yt3.ggpht.com/-RhFrdqLtqTU/AAAAAAAAAAI/AAAAAAAAAAA/CO09m4_x86c/s288-mo-c-c0xffffffff-rj-k-no/photo.jpg',
                access_token: 'AIzaSyDEWIzmhAE16S6p9uALPu0I6IJtmXX5La8'  //String: "APP_ID|APP_SECRET"
            },
                       
            template: "<?= base_url() ?>js/social/js/template.html",                         //String: Filename used to get the post template.            
            date_format: "ll",                              //String: Display format of the date attribute (see http://momentjs.com/docs/#/displaying/format/)
            date_locale: "en",                              //String: The locale of the date (see: http://momentjs.com/docs/#/i18n/changing-locale/)
            moderation: function(content) {                 //Function: if returns false, template will have class hidden
                return  (content.text) ? content.text.indexOf('fuck') == -1 : true;
            }            
        });
    });
</script>
