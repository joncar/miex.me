<div class="loaded" id="page-content"> 
    <header class="overlay"> <!-- navigation / main menu --> 
        <?= $this->load->view('includes/template/menu2') ?>
    </header> <!-- main content --> 
    <main> 
        <section style="padding-top: 117px; height: 100%;"> 
            <div class="container">
                <h1>Zona Usuario</h1>
                <div class="row" style="margin:20px;">
                    <div class="col-xs-12 col-sm-2">
                        <img src="<?= base_url(empty($this->user->foto)?'assets/grocery_crud/css/jquery_plugins/cropper/vacio.png':'img/fotos/'.$this->user->foto) ?>" alt="<?= $this->user->nombre ?>" style="width:100%; border:5px solid #f71259"/>
                    </div>
                    <div class="col-xs-12 col-sm-8 panelUsuarioDatos" style="border-right:5px solid #f71259;">
                        <ul class="list-group">
                            <li class="list-group-item"><b>Nombre: </b><?= $this->user->nombre ?></li>
                            <li class="list-group-item"><b>Apellido: </b><?= $this->user->apellido ?></li>
                            <li class="list-group-item"><b>Centro: </b><?= $this->user->centro ?></li>
                            <li class="list-group-item"><b>Curso: </b><?= $this->user->curso ?></li>
                            <li class="list-group-item"><b>Grupo: </b><?= $this->user->nro_grupo ?></li>
                        </ul>
                    </div>
                    <div class="col-xs-12 col-sm-2">
                        <ul class="list-group">
						<li class="list-group-item" style="background-color: #f72859; color: white;">Documentos</li>                            
						  <?php foreach($this->db->get('documentos')->result() as $d): ?>
                            <li class="list-group-item">
                                <!--<a href="<?= base_url('files/'.$d->file) ?>" target="_new"><?= $d->nombre ?></a>-->
                                <a href="javascript:popup('<?= $d->nombre ?>','<?= base_url('files/'.$d->file) ?>')"><?= $d->nombre ?></a>
                            </li>
                          <?php endforeach ?>
                        </ul>
                    </div>
                </div>
                <div class="row" style="margin:20px;">
                    <h2>Instrucciones</h2>
                    <p>Lorem Ipsu Cate Lorem Ipsu Cate Lorem Ipsu Cate Lorem Ipsu Cate Lorem Ipsu Cate Lorem Ipsu Cate Lorem Ipsu Cate Lorem Ipsu Cate Lorem Ipsu Cate </p>
                </div>
                <div class="row" style="margin:20px;">
                    <?= $output ?>
                </div>
            </div> 
        </section> <!-- go up arrow --> 
        <button class="btn goUp-btn"> 
            <i class="fa fa-angle-up"></i> <span>Go Up</span><span class="mydiv">variolitic</span> 
        </button> <!-- /.go up arrow --> 
        <?php $this->load->view('includes/scripts',array('removeFunction'=>true)); ?>
    </main>
    <!-- Prueba --<form id="formPay" action="https://tpv.ceca.es/tpvweb/tpv/compra.action" method="post" enctype="application/x-www-form-urlencoded"> -->
    <form id="formPay" action="https://pgw.ceca.es/cgi-bin/tpv" method="post" enctype="application/x-www-form-urlencoded">
        <input name="MerchantID" type=hidden value="081534372">
        <input name="AcquirerBIN" type=hidden value="0000554026">
        <input name="TerminalID" type=hidden value="00000003">
        <input name="URL_OK" type=hidden value="<?= base_url('pagos/frontend/pagook') ?>">
        <input name="URL_NOK" type=hidden value="<?= base_url('pagos/frontend/pagonok') ?>">
        <input id="firma" name="Firma" type=hidden value="">
        <input name="Cifrado" type=hidden value="SHA1">
        <input id="operacion" name="Num_operacion" type=hidden value="">
        <input id="importe" name="Importe" type=hidden value="">
        <input name="TipoMoneda" type=hidden value="978">
        <input name="Exponente" type=hidden value="2">
        <input name="Pago_soportado" type=hidden value="SSL">
        <input name="Idioma" type=hidden value="1">
    </form>
</div>

<div class="modal" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="false">
    
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <p class="modal-title" id="myModalLabel"><b>Documento y autorización</b></p>
            </div>
            <div class="modal-body">
                <p><b>Instrucciones</b></p>
                <ol>
                    <li>Descarga la <a id="file" href="#" target="_new" style="text-decoration: underline;">planilla editable</a> a tu escritorio <a href="#" target="_new" id="file2"><i class="fa fa-download"></i></a></li>
                    <li>Edítala con algún visor PDF y guardala</li>
                    <li>Guarda los cambios</li>
                    <li>Envíala por E-mail a <a href="mailto:info@miex.me" style="text-decoration: underline;">info@miex.me</a></li>                    
                </ol>
            </div>
            <div class="modal-footer">
                <button type="button" onclick="$('#myModal1').toggle()" class="btn btn-danger" data-dismiss="modal">Cerrar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
    <div class="modal-backdrop"></div>
</div>


<script>
    $('#crudForm').on('success',function(ev,e){
        tokenizar(e);
    });
    
    function tokenizar(id){
        $.post('<?= base_url('pagos/admin/tokenizar_pago') ?>',{id:id},function(data){
            data = JSON.parse(data);
            $("#importe").val(data.importe);
            $("#operacion").val(data.id);
            $("#firma").val(data.firma);
            $("#formPay").submit();
        });
    }
    
    function popup(name,file){
        $("#myModal1 #myModalLabel").html(name);
        $("#myModal1 #file").attr('href',file);
        $("#myModal1 #file2").attr('href',file);
        $("#myModal1").toggle();
    }
</script>