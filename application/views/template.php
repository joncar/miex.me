<!DOCTYPE html>
<html lang="en">
    <head>
        <style type="text/css">
            .gm-style-pbc{transition:opacity ease-in-out;background-color:black;text-align:center}
            .gm-style-pbt{
               font-size:22px;color:white;font-family:Roboto,Arial,sans-serif;position:relative;
               margin:0;top:50%;-webkit-transform:translateY(-50%);-ms-transform:translateY(-50%);
               transform:translateY(-50%)}
        </style>
        <link type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=Roboto:300,400,500,700">
        <style type="text/css">.gm-style .gm-style-cc span,.gm-style .gm-style-cc a,.gm-style .gm-style-mtc div{font-size:10px}</style>
        <style type="text/css">@media print {  .gm-style .gmnoprint, .gmnoprint {    display:none  }}@media screen {  .gm-style .gmnoscreen, .gmnoscreen {    display:none  }}</style><style type="text/css">.gm-style{font-family:Roboto,Arial,sans-serif;font-size:11px;font-weight:400;text-decoration:none}.gm-style img{max-width:none}</style> 
        <meta charset="utf-8"> 
        <meta content="IE=edge" http-equiv="X-UA-Compatible"> 
        <meta content="width=device-width, initial-scale=1" name="viewport"> 
        <title><?= empty($title)?'MIEX.ME':$title ?></title> <!-- fonts --> 
        <link type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=Raleway:400,900,700"> 
        <link type="text/css" rel="stylesheet" href="//fonts.googleapis.com/css?family=PT+Serif:400,700"> <!-- stylesheets --> 
        <link href="<?= base_url() ?>css/plugins.css" rel="stylesheet"> 
        <link href="<?= base_url() ?>css/owl.carousel.css" rel="stylesheet"> 
        <?php if(!empty($scripts)): ?>
            <?= $scripts ?>
        <?php endif ?>
        <link href="<?= base_url() ?>css/styles.css" rel="stylesheet"> 
        <link href="<?= base_url() ?>css/mediaqueries.css" rel="stylesheet"> <!-- fonts stylesheets --> 
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
        <link href="<?= base_url() ?>css/fonts.css" rel="stylesheet"> 
        <!-- Social-feed css -->
        <link href="<?= base_url() ?>css/social.css" rel="stylesheet" type="text/css">                  
        <script>
            var base_url = '<?= base_url() ?>';
        </script>
    </head>
    <body>
            <?= $this->load->view($view); ?>
            <script src="<?= base_url('js/owl.carousel.js') ?>"></script>
            <script src="<?= base_url('js/frame.js') ?>"></script>
    </body>
    </html>