<!Doctype html>
<html lang="es">
	<head>
		<title><?= empty($title)?'Academico':$title ?></title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
                                    <meta http-equiv="X-UA-Compatible" content="IE=edge">
		<?php 
		if(!empty($css_files) && !empty($js_files)):
		foreach($css_files as $file): ?>
		<link type="text/css" rel="stylesheet" href="<?= $file ?>" />
		<?php endforeach; ?>
		<?php foreach($js_files as $file): ?>
		<script src="<?= $file ?>"></script>
		<?php endforeach; ?>                
                <?php endif; ?>                                
                <? if(empty($crud) || empty($css_files)): ?>
                <script src="http://code.jquery.com/jquery-1.10.0.js"></script>		
                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
                <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
                <? endif ?>
                <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">                
                <link rel="stylesheet" type="text/css" href="<?= base_url('css/ace.min.css') ?>">
                <script src="<?= base_url('js/ace-extra.min.js') ?>"></script>		                
        </head>
        <body class="no-skin">
		<div id="navbar" class="navbar navbar-default">
			<script type="text/javascript">
				try{ace.settings.check('navbar' , 'fixed')}catch(e){}
			</script>

			<div class="navbar-container" id="navbar-container">
				<button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
					<span class="sr-only">Toggle sidebar</span>

					<span class="icon-bar"></span>

					<span class="icon-bar"></span>

					<span class="icon-bar"></span>
				</button>

				<div class="navbar-header pull-left">
					<a href="<?= site_url() ?>" class="navbar-brand">
						<small>
							<i class="fa fa-leaf"></i>
							Universidad Nacional de Pilar
						</small>
					</a>
				</div>

				<div class="navbar-buttons navbar-header pull-right" role="navigation">
					<ul class="nav ace-nav">
						<li class="grey">
							<a data-toggle="dropdown" class="dropdown-toggle" href="#">
								<i class="ace-icon fa fa-tasks"></i>
								<span class="badge badge-grey">0</span>
							</a>

							<ul class="dropdown-menu-right dropdown-navbar dropdown-menu dropdown-caret dropdown-close">
								<li class="dropdown-header">
									Sin tareas pendientes
								</li>	
                                                                <li class="dropdown-content">
									<ul class="dropdown-menu dropdown-navbar navbar-pink">
										<li>Sin tareas pendientes</li>
									</ul>
								</li>
							</ul>
						</li>

						<li class="purple">
							<a data-toggle="dropdown" class="dropdown-toggle" href="#">
								<i class="ace-icon fa fa-bell"></i>
								<span class="badge badge-important">0</span>
							</a>

							<ul class="dropdown-menu-right dropdown-navbar navbar-pink dropdown-menu dropdown-caret dropdown-close">
								<li class="dropdown-header">
									<i class="ace-icon fa fa-exclamation-triangle"></i>
									0 Notificaciones
								</li>

								<li class="dropdown-content">
									<ul class="dropdown-menu dropdown-navbar navbar-pink">
										<li>Sin notificaciones pendientes</li>
									</ul>
								</li>							
							</ul>
						</li>

						<li class="green">
							<a data-toggle="dropdown" class="dropdown-toggle" href="#">
								<i class="ace-icon fa fa-envelope"></i>
								<span class="badge badge-success">0</span>
							</a>

							<ul class="dropdown-menu-right dropdown-navbar dropdown-menu dropdown-caret dropdown-close">
								<li class="dropdown-header">
									<i class="ace-icon fa fa-envelope-o"></i>
									Sin mensajes
								</li>

								<li class="dropdown-content">
									<ul class="dropdown-menu dropdown-navbar">
                                                                            <li>Sin Mensajes</li>
									</ul>
								</li>
							</ul>
						</li>

						<li class="light-blue">
							<a data-toggle="dropdown" href="#" class="dropdown-toggle">
								<img class="nav-user-photo" src="assets/avatars/user.jpg" alt="Jason's Photo" />
								<span class="user-info">
									<small>Bienvenido</small>
									Jonathan
								</span>
								<i class="ace-icon fa fa-caret-down"></i>
							</a>

							<ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
								<li>
									<a href="#">
										<i class="ace-icon fa fa-cog"></i>
										Configuración
									</a>
								</li>

								<li>
									<a href="profile.html">
										<i class="ace-icon fa fa-user"></i>
										Perfil
									</a>
								</li>
								<li>
									<a href="#">
										<i class="ace-icon fa fa-power-off"></i>
										Salir
									</a>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</div><!-- /.navbar-container -->
		</div>

		<div class="main-container" id="main-container">
			<div id="sidebar" class="sidebar responsive">
				<ul class="nav nav-list">
					<li class="active highlight">
						<a href="index.html">
							<i class="menu-icon fa fa-tachometer"></i>
							<span class="menu-text">Escritorio</span>
						</a>

						<b class="arrow"></b>
					</li>
				</ul><!-- /.nav-list -->			
			</div>

			<div class="main-content">
				<div class="main-content-inner">
                                    <div class="breadcrumbs" id="breadcrumbs">
                                            <ul class="breadcrumb">
                                                    <li>
                                                            <i class="ace-icon fa fa-home home-icon"></i>
                                                            <a href="#">Home</a>
                                                    </li>
                                                    <li class="active"><?= empty($title)?'Escritorio':$title ?></li>
                                            </ul><!-- /.breadcrumb -->
                                    </div>

                                    <div class="page-content">						
                                            <div class="page-header">
                                                    <h1>
                                                            Escritorio
                                                            <small>
                                                                    <i class="ace-icon fa fa-angle-double-right"></i>
                                                                    Estadisticas
                                                            </small>
                                                    </h1>
                                            </div><!-- /.page-header -->

                                            <div class="row">
                                                    <div class="col-xs-12">
                                                            <!-- PAGE CONTENT BEGINS -->

                                                    </div><!-- /.col -->
                                            </div><!-- /.row -->
                                    </div><!-- /.page-content -->
				</div>
			</div><!-- /.main-content -->			
		</div><!-- /.main-container -->
        </body>
</html>
